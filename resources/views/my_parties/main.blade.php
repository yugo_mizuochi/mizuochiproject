@extends('common.templete')

@section('title', '登録パーティー一覧')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">
			@component('common.flash_message')@endcomponent

			@component('my_parties.menu')@endcomponent

			@component('my_parties.my_parties_table', ['myParties' => $vm->myParties])@endcomponent

			@component('my_parties.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection