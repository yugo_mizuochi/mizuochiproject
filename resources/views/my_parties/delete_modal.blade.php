<div class="delete_my_party_modal_content">
	<div class="overlay">
		<div class="delete_my_party_modal">
			<form action="{{ route('delete_my_party') }}" method="post">
				@csrf
				<div class="delete_my_party_modal_text">
					{{ $myParty->getName() . 'を削除しますか？' }}
				</div>
				<input type="hidden" name="my_party_id" value="{{ $myParty->getId() }}">
				<div class="delete_my_party_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_delete_my_party">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>