<section>

	<h2>登録パーティー一覧</h2>

	@foreach($myParties as $myParty)
		<table class="ta1">
			<thead>
				<tr>
					<th colspan="6">{{ $myParty->getName() }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getFirstMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getSecondMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getThirdMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getFourthMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getFifthMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $myParty->getSixthMyPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
				</tr>
				<tr>
					<td>{{ $myParty->getFirstMyPokemon()->getMyPokemonName() }}</td>
					<td>{{ $myParty->getSecondMyPokemon()->getMyPokemonName() }}</td>
					<td>{{ $myParty->getThirdMyPokemon()->getMyPokemonName() }}</td>
					<td>{{ $myParty->getFourthMyPokemon()->getMyPokemonName() }}</td>
					<td>{{ $myParty->getFifthMyPokemon()->getMyPokemonName() }}</td>
					<td>{{ $myParty->getSixthMyPokemon()->getMyPokemonName() }}</td>
				</tr>
				<tr>
					<td>{{ $myParty->getFirstMyPokemon()->getItem()->getName() }}</td>
					<td>{{ $myParty->getSecondMyPokemon()->getItem()->getName() }}</td>
					<td>{{ $myParty->getThirdMyPokemon()->getItem()->getName() }}</td>
					<td>{{ $myParty->getFourthMyPokemon()->getItem()->getName() }}</td>
					<td>{{ $myParty->getFifthMyPokemon()->getItem()->getName() }}</td>
					<td>{{ $myParty->getSixthMyPokemon()->getItem()->getName() }}</td>
				</tr>
				<tr>
					<td colspan="6">{{ $myParty->getMemo() }}</td>
				</tr>
				<tr>
					<td colspan="6">
						<a href="{{ route('edit_my_party_form', ['myPartyId' => $myParty->getId()]) }}" class="edit_my_party_form_button">編集</a>
					</td>
				</tr>
				<tr>
					<td colspan="6"><button class="delete_my_party_modal_button">削除</button></td>
					@component('my_parties.delete_modal', ['myParty' => $myParty])@endcomponent
				</tr>
			</tbody>
		</table>
	@endforeach

</section>

{{ $myParties->getLink() }}