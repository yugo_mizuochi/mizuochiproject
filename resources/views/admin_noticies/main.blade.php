@extends('common.templete')

@section('title', 'お知らせ一覧')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('common.flash_message')@endcomponent
			
			@component('admin_noticies.menu')@endcomponent

			@component(
				'admin_noticies.noticies_table', 
				[
					'noticies' => $vm->noticies
				]
			)@endcomponent

			@component('admin_noticies.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection