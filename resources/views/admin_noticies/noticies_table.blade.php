<section>
	
	<h2>お知らせ一覧</h2>

	<table class="ta1">
		<thead>
			<tr>
				<th>ID</th>
				<th>本文</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($noticies as $notice)
				<tr>
					<td>{{ $notice->getId() }}</td>
					<td>{{ $notice->getBody() }}</td>
					<td><a href="{{ route('edit_notice_form', ['noticeId' => $notice->getId()]) }}" class="edit_notice_form_button">編集</a></td>
					<td>
						<button class="delete_notice_modal_button">削除</button>
						@component(
							'admin_noticies.delete_modal',
							[
								'notice' => $notice
							]
						)@endcomponent
					</td>
				</tr>
			@endforeach
		</tbody> 
	</table>
</section>

{{ $noticies->getLink() }}