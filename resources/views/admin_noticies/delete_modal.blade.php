<div class="delete_notice_modal_content">
	<div class="overlay">
		<div class="delete_notice_modal">
			<form action="{{ route('delete_notice') }}" method="post">
				@csrf
				<div class="delete_notice_modal_text">
					{{ 'お知らせ番号' . $notice->getId() . 'を削除しますか?' }}
				</div>
				<input type="hidden" name="notice_id" value="{{ $notice->getId() }}">
				<div class="delete_notice_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_delete_notice">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>