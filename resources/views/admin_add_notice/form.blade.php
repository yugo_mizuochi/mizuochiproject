<section>
	<h2>お知らせ新規登録フォーム</h2>

	<form action="{{ route('add_notice') }}" method="post">

		@csrf

		<table>
			<tr>
				<th>本文</th>
				<td>
					<textarea type="text" name="notice_body" required>{{ old('notice_body') }}</textarea>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div id="add_notice_buttons">
			<button type="submit">登録</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
		
	</form>
</section>