@extends('common.templete')

@section('title', 'お知らせ新規登録')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">
			
			@component('admin_add_notice.form')@endcomponent

			@component('admin_add_notice.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection