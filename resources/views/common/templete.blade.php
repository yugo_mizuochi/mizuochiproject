<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="{{ asset('css/common/style.css') }}">
		<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
	</head>

	<body>

		@yield('nav')

		@yield('content')

		@yield('footer')

	</body>
</html>
