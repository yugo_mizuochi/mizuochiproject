<?php
	use App\Enums\UserType;
	use Illuminate\Support\Facades\Auth;
	use App\Model\User;

	$userType = User::find(Auth::id())->user_type;
?>

@section('nav')

	<div id="sub">

		<p id="logo"><a href="{{ url('home') }}"><img src="{{ asset('img/icon.jpeg') }}"></a></p>

		<nav id="menubar">
			<ul>
				<li><a href="{{ route('home') }}">ホーム画面</a></li>
				<li><a href="{{ route('my_pokemons') }}">登録ポケモン一覧</a></li>
				<li><a href="{{ route('my_parties') }}">パーティー一覧</a></li>
				<li><a href="{{ route('user_pokemons') }}">ポケモンを探す</a></li>
				<li><a href="{{ route('favorite_my_pokemons') }}">お気に入りポケモン一覧</a></li>
				<li><a href="{{ route('user_parties') }}">パーティーを探す</a></li>
				<li><a href="{{ route('favorite_my_parties') }}">お気に入りパーティー一覧</a></li>
				@if($userType === UserType::ADMIN)
					<li><a href="{{ route('admin_noticies') }}">管理側お知らせ一覧へ</a></li>
				@endif
				<li><a href="{{ Route('logout') }}">ログアウト</a></li>
			</ul>
		</nav>

	</div>
	<!--/sub-->

@endsection