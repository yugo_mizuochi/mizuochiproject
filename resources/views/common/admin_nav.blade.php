@section('nav')

	<div id="sub">

		<p id="logo"><a href="{{ url('home') }}"><img src="{{ asset('img/icon.jpeg') }}"></a></p>

		<nav id="menubar">
			<ul>
				<li><a href="{{ route('home') }}">クライアント画面へ</a></li>
				<li><a href="{{ route('admin_noticies') }}">お知らせ一覧</a></li>
				<li><a href="{{ route('admin_users') }}">ユーザー一覧</a></li>
				<li><a href="{{ Route('logout') }}">ログアウト</a></li>
			</ul>
		</nav>

	</div>
	<!--/sub-->

@endsection