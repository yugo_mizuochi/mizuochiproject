<div class="delete_modal_content">
	<div class="overlay">
		<div class="delete_my_pokemon_modal">
			<form action="{{ route('delete_my_pokemon') }}" method="post">
				@csrf
				<div class="delete_modal_text">
					{{ $myPokemon->getMyPokemonName() . 'を削除しますか？' }}
				</div>
				<input type="hidden" name="my_pokemon_id" value="{{ $myPokemon->getId() }}">
				<div class="delete_my_pokemon_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_delete_my_pokemon">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>