@extends('common.templete')

@section('title', '登録ポケモン一覧')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('common.form_errors')@endcomponent

			@component('common.flash_message')@endcomponent

			@component('mypokemons.menu')@endcomponent

			@component('mypokemons.my_pokemons_table', ['myPokemons' => $vm->myPokemons])@endcomponent

			@component('mypokemons.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection