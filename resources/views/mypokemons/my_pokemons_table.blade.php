<section>

	<h2>登録ポケモン一覧</h2>

	@foreach($myPokemons as $myPokemon)
		<table class="ta1">
			<thead>
				<tr>
					<th></th>
					<th>ポケモン名</th>
					<th>命名</th>
					<th>持ち物</th>
					<th>HP</th>
					<th>攻撃</th>
					<th>防御</th>
					<th>特攻</th>
					<th>特防</th>
					<th>素早さ</th>
					<th>編集</th>
					<th>削除</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $myPokemon->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>{{ $myPokemon->getPokemon()->getName() }}</td>
					<td class="my_pokemon_name">{{ $myPokemon->getMyPokemonName() }}</td>
					<td>{{ $myPokemon->getItem()->getName() }}</td>
					<td>{{ $myPokemon->getHp() }}</td>
					<td>{{ $myPokemon->getAttack() }}</td>
					<td>{{ $myPokemon->getDefense() }}</td>
					<td>{{ $myPokemon->getSpecialAttack() }}</td>
					<td>{{ $myPokemon->getSpecialDefense() }}</td>
					<td>{{ $myPokemon->getAgility() }}</td>
					<td><a href="{{ route('edit_my_pokemon.form', ['myPokemonId' => $myPokemon->getId()]) }}">編集</a></td>
					<td><button class="delete_my_pokemon_modal_button">削除</button></td>
				</tr>
				<tr>
					<td>メモ</td>
					<td colspan="12">{{ $myPokemon->getMemo() }}</td>
				</tr>
			</tbody>
		</table>
		@component('mypokemons.delete_modal', ['myPokemon' => $myPokemon])@endcomponent
	@endforeach

</section>

{{ $myPokemons->getLink() }}