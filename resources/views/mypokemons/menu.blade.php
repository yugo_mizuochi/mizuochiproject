<section>
	<h2>登録ポケモンメニュー</h2>
	<div id="my_pokemons_menu">
		<div class="inline_my_pokemons_menu">
			<a href="{{ route('add_my_pokemon.form') }}">新規登録</a>
		</div>
		<div class="inline_my_pokemons_menu">
			<a href="{{ route('favorite_my_pokemons') }}">お気に入り</a>
		</div>
		<div class="inline_my_pokemons_menu">
			<a href="{{ route('my_parties') }}">パーティー</a>
		</div>
	</div>
</section>