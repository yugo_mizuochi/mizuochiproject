<div class="release_favorite_my_party_modal_content">
	<div class="overlay">
		<div class="release_favorite_my_party_modal">
			<form action="{{ route('release_favorite_my_party') }}" method="post">
				@csrf
				<div class="release_favorite_my_party_modal_text">
					{{ $favoriteMyParty->getUserParty()->getUserPartyName() . 'をお気に入り解除しますか？' }}
				</div>
				<input type="hidden" name="favorite_party_id" value="{{ $favoriteMyParty->getId() }}">
				<div class="release_favorite_my_party_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_release_favorite_my_party">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>