<section>

	<h2>お気に入りパーティー一覧</h2>

	@foreach($favoriteMyParties as $favoriteMyParty)
		<table class="ta1">
			<thead>
				<tr>
					<th colspan="6">{{ $favoriteMyParty->getUserParty()->getUserPartyName() }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getFirstUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getSecondUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getThirdUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getFourthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getFifthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyParty->getUserParty()->getSixthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
				</tr>
				<tr>
					<td>{{ $favoriteMyParty->getUserParty()->getFirstUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getSecondUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getThirdUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getFourthUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getFifthUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getSixthUserPokemon()->getUserPokemonName() }}</td>
				</tr>
				<tr>
					<td>{{ $favoriteMyParty->getUserParty()->getFirstUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getSecondUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getThirdUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getFourthUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getFifthUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyParty->getUserParty()->getSixthUserPokemon()->getItem()->getName() }}</td>
				</tr>
				<tr>
					<td colspan="6">{{ $favoriteMyParty->getUserParty()->getMemo() }}</td>
				</tr>
				<tr>
					<td colspan="6">
						<button class="add_favorite_my_party_modal_button">登録</button>
						@component(
							'favorite_my_parties.add_modal',
							[
								'favoriteMyParty' => $favoriteMyParty
							]
						)@endcomponent
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<button class="release_favorite_my_party_modal_button">解除</button>
						@component(
							'favorite_my_parties.release_modal',
							[
								'favoriteMyParty' => $favoriteMyParty
							]
						)@endcomponent
					</td>
				</tr>
			</tbody>
		</table>
	@endforeach

</section>

{{ $favoriteMyParties->getLink() }}