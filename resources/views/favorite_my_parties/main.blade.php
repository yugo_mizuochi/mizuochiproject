@extends('common.templete')

@section('title', 'お気に入りパーティー一覧')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('common.flash_message')@endcomponent

			@component(
				'favorite_my_parties.favorite_my_parties_table',
				[
					'favoriteMyParties' => $vm->favoriteMyParties
				]
			)@endcomponent

			@component('favorite_my_parties.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection