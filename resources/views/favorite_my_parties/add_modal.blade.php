<div class="add_favorite_my_party_modal_content">
	<div class="overlay">
		<div class="add_favorite_my_party_modal">
			<form action="{{ route('add_favorite_to_my_party') }}" method="post">
				@csrf
				<div class="add_favorite_my_party_modal_text">
					{{ $favoriteMyParty->getUserParty()->getUserPartyName() . 'をあなたのパーティーに登録しますか？' }}
				</div>
				<input type="hidden" name="favorite_party_id" value="{{ $favoriteMyParty->getId() }}">
				<div class="add_favorite_my_party_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_add_favorite_my_party">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>