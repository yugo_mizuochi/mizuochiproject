@extends('common.templete')

@section('title', 'パーティーを探す')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">
			@component('common.flash_message')@endcomponent

			@component(
				'user_parties.search_box',
				[
					'pokemons' => $vm->pokemons,
					'searchStatus' => $vm->searchId
				]
			)@endcomponent

			@component(
				'user_parties.user_parties_table',
				[
					'userParties' => $vm->userParties
				]
			)@endcomponent

			@component('user_parties.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection