<div class="release_user_party_modal_content">
	<div class="overlay">
		<div class="release_user_party_modal">
			<form action="{{ route('release_favorite_user_party') }}" method="post">
				@csrf
				<div class="release_modal_text">
					{{ $userParty->getUserPartyName() . 'をお気に入り解除しますか？' }}
				</div>
				<input type="hidden" name="user_party_id" value="{{ $userParty->getId() }}">
				<div class="release_user_party_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_release_user_party">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>