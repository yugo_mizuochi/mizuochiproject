<section>

	<h2>検索結果 {{ $userParties->getTotal() . '件' }}</h2>

	@foreach($userParties as $userParty)
		<table class="ta1">
			<thead>
				<tr>
					<th colspan="6">{{ $userParty->getUserPartyName() }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getFirstUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getSecondUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getThirdUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getFourthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getFifthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>
						<img src="{{ asset('img/sprites/' . $userParty->getSixthUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
				</tr>
				<tr>
					<td>{{ $userParty->getFirstUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $userParty->getSecondUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $userParty->getThirdUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $userParty->getFourthUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $userParty->getFifthUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $userParty->getSixthUserPokemon()->getUserPokemonName() }}</td>
				</tr>
				<tr>
					<td>{{ $userParty->getFirstUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $userParty->getSecondUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $userParty->getThirdUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $userParty->getFourthUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $userParty->getFifthUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $userParty->getSixthUserPokemon()->getItem()->getName() }}</td>
				</tr>
				<tr>
					<td colspan="6">{{ $userParty->getMemo() }}</td>
				</tr>
				<tr>
					<td colspan="6">
						<button class={{ $userParty->getModalButtonClass() }}>{{ $userParty->getFavoriteStatus() }}</button>
						@component('user_parties.' . $userParty->getModalName(), ['userParty' => $userParty])@endcomponent
					</td>
				</tr>
			</tbody>
		</table>
	@endforeach

</section>

{{ $userParties->getLink() }}