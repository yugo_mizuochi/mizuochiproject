<section>
	<h2>ユーザー編集フォーム</h2>

	<form action="{{ route('edit_user', ['userId' => $user->getId()]) }}" method="post">

		@csrf

		<table>
			<tr>
				<th>名前</th>
				<td>
					<input 
						type="text" 
						name="user_name"  
						value="{{ count($errors) > 0 ? old('user_name') : $user->getName() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>Eメール</th>
				<td>
					<input 
						type="email" 
						name="user_email" 
						value="{{ count($errors) > 0 ? old('user_email') : $user->getEmail() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>属性</th>
				<td>
					<select name="user_type">
						@foreach ($userTypes as $key => $userType)
							<option
								value="{{ $key }}" 
								{{ $key === old('user_type') || $userType === $user->getUserType() ? 'selected' : '' }}
							>
								{{ $userType }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div class="edit_user_buttons">
			<input type="hidden" name="user_id" value="{{ $user->getId() }}">
			<button type="submit">編集</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
		
	</form>
</section>