@extends('common.templete')

@section('title', 'ユーザー編集')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'admin_edit_user.form',
				[
					'user' => $vm->user,
					'userTypes' => $vm->userTypes
				]
			)@endcomponent

			@component('admin_edit_user.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection