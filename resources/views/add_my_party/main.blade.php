@extends('common.templete')

@section('title', 'パーティー新規登録')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'add_my_party.form',
				[
					'myPokemons' => $vm->myPokemons
				]
			)@endcomponent

			@component('add_my_party.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection