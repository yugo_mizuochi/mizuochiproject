@extends('common.templete')

@section('title', 'ユーザー一覧')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('common.form_errors')@endcomponent

			@component('common.flash_message')@endcomponent
			
			@component('admin_users.menu')@endcomponent

			@component(
				'admin_users.users_table',
				[
					'users' => $vm->users
				]
			)@endcomponent

			@component('admin_users.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection