<section>
	
	<h2>ユーザー一覧</h2>

	<table class="ta1">
		<thead>
			<tr>
				<th>ID</th>
				<th>名前</th>
				<th>Eメール</th>
				<th>属性</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->getId() }}</td>
					<td>{{ $user->getName() }}</td>
					<td>{{ $user->getEmail() }}</td>
					<td>{{ $user->getUserType() }}</td>
					<td>
						<a href="{{ route('edit_user_form', ['userId' => $user->getId()]) }}" class="edit_user_form_button">編集</a>
					</td>
					<td>
						<button class="delete_user_modal_button">削除</button>
						@component(
							'admin_users.delete_modal',
							[
								'user' => $user
							]
						)@endcomponent
					</td>
				</tr>
			@endforeach
		</tbody> 
	</table>
</section>

{{ $users->getLink() }}