<div class="delete_user_modal_content">
	<div class="overlay">
		<div class="delete_user_modal">
			<form action="{{ route('delete_user') }}" method="post">
				@csrf
				<div class="delete_user_modal_text">
					{{ $user->getName() . 'さんを削除しますか?' }}
				</div>
				<input type="hidden" name="user_id" value="{{ $user->getId() }}">
				<div class="delete_user_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_delete_user">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>