<section id="new" class="box">
	<h2>更新情報・お知らせ</h2>
	<dl id="newinfo">
		@foreach($noticies as $notice)
			<dt>{{ $notice->getDate() }}</dt>
			<dd>{{ $notice->getBody() }} @if($loop->first)<span class="newicon">NEW</span> @endif</dd>
		@endforeach					
	</dl>
</section>