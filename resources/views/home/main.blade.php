@extends('common.templete')

@section('title', 'home画面')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('home.noticies_box', ['noticies' => $vm->noticies])@endcomponent

			@component('home.introduction_box')@endcomponent

			@component('home.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection