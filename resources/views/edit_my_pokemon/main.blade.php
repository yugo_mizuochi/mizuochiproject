@extends('common.templete')

@section('title', 'ポケモン編集')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'edit_my_pokemon.form',
				[
					'myPokemon' => $vm->myPokemon,
					'items' => $vm->items,
					'pokemons' => $vm->pokemons
				]
			)@endcomponent

			@component('edit_my_pokemon.asset')@endcomponent
			
		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection