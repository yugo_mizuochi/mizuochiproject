<section>
	<h2>編集フォーム</h2>	

	<form action="{{ route('edit_my_pokemon', ['myPokemonId' => $myPokemon->getId()]) }}" method="post">

		@csrf

		<table>
			<tr>
				<th>ポケモン</th>
				<td>
					<select name="pokemon_id" required>
						@foreach($pokemons as $pokemon)
							<option 
								value="{{ $pokemon->getId() }}"
								{{ $pokemon->getId() == $myPokemon->getPokemon()->getId() || $pokemon->getId() == old('pokemon_id') ? 'selected' : '' }}
							>
								{{ $pokemon->getName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン命名</th>
				<td>
					<input 
						type="text" 
						name="my_pokemon_name" 
						value="{{ count($errors) > 0 ? old('my_pokemon_name') : $myPokemon->getMyPokemonName() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>持ち物</th>
				<td>
					<select name="item_id" required>
						@foreach($items as $item)
							<option 
								value="{{ $item->getId() }}"
								{{ $item->getId() == $myPokemon->getItem()->getId() || $item->getId() == old('item_id') ? 'selected' : '' }}
							>
								{{ $item->getName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>HP努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[hp_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.hp_value') : $myPokemon->getHp() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>攻撃努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[attack_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.attack_value') : $myPokemon->getAttack() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>防御努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[defense_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.defense_value') : $myPokemon->getDefense() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>特攻努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[special_attack_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.special_attack_value') : $myPokemon->getSpecialAttack() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>特防努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[special_defense_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.special_defense_value') : $myPokemon->getSpecialDefense() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>素早さ努力値</th>
				<td>
					<input 
						type="number" 
						name="effort_values[agility_value]" 
						value="{{ count($errors) > 0 ? old('effort_values.agility_value') : $myPokemon->getAgility() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>メモ</th>
				<td>
					<textarea 
						type="text" 
						name="my_pokemon_memo" 
						required
					>{{ count($errors) > 0 ? old('my_pokemon_memo') : $myPokemon->getMemo() }}</textarea>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div id="edit_mypokemon_buttons">
			<button type="submit">編集</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
	</form>
</section>