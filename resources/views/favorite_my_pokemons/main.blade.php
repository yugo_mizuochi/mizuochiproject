@extends('common.templete')

@section('title', 'お気に入りポケモン一覧')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component('common.flash_message')@endcomponent

			@component(
				'favorite_my_pokemons.favorite_my_pokemons_table',
				[
					'favoriteMyPokemons' => $vm->favoriteMyPokemons
				]
			)@endcomponent

			@component('favorite_my_pokemons.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection