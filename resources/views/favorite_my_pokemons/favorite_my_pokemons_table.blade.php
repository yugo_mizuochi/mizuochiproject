<section>

	<h2>お気に入りポケモン一覧</h2>

	@foreach($favoriteMyPokemons as $favoriteMyPokemon)
		<table class="ta1">
			<thead>
				<tr>
					<th></th>
					<th>ポケモン名</th>
					<th>命名</th>
					<th>持ち物</th>
					<th>HP</th>
					<th>攻撃</th>
					<th>防御</th>
					<th>特攻</th>
					<th>特防</th>
					<th>素早さ</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $favoriteMyPokemon->getUserPokemon()->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getPokemon()->getName() }}</td>
					<td class="favorite_my_pokemon_name">{{ $favoriteMyPokemon->getUserPokemon()->getUserPokemonName() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getItem()->getName() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getHp() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getAttack() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getDefense() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getSpecialAttack() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getSpecialDefense() }}</td>
					<td>{{ $favoriteMyPokemon->getUserPokemon()->getAgility() }}</td>
					<td>
						<button class="add_favorite_my_pokemon_modal_button">登録</button>
						@component(
							'favorite_my_pokemons.add_favorite_my_pokemon_modal',
							[
								'favoriteMyPokemon' => $favoriteMyPokemon
							]
						)@endcomponent
					</td>
					<td>
						<button class="release_favorite_my_pokemon_modal_button">解除</button>
						@component(
							'favorite_my_pokemons.release_favorite_my_pokemon_modal',
							[
								'favoriteMyPokemon' => $favoriteMyPokemon
							]
						)@endcomponent
					</td>
				</tr>
				<tr>
					<td>メモ</td>
					<td colspan="12">{{ $favoriteMyPokemon->getUserPokemon()->getMemo() }}</td>
				</tr>
			</tbody>
		</table>
	@endforeach

</section>

{{ $favoriteMyPokemons->getLink() }}