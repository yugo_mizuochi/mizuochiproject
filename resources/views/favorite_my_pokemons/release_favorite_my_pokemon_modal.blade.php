<div class="release_favorite_my_pokemon_modal_content">
	<div class="overlay">
		<div class="release_favorite_my_pokemon_modal">
			<form action="{{ route('release_favorite_my_pokemon') }}" method="post">
				@csrf
				<div class="release_favorite_my_pokemon_modal_text">
					{{ $favoriteMyPokemon->getUserPokemon()->getUserPokemonName() . 'をお気に入り解除しますか？' }}
				</div>
				<input type="hidden" name="favorite_id" value="{{ $favoriteMyPokemon->getId() }}">
				<div class="release_favorite_my_pokemon_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_release_favorite_my_pokemon">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>