<div class="add_favorite_my_pokemon_modal_content">
	<div class="overlay">
		<div class="add_favorite_my_pokemon_modal">
			<form action="{{ route('add_favorite_to_my_pokemon') }}" method="post">
				@csrf
				<div class="add_favorite_my_pokemon_modal_text">
					{{ $favoriteMyPokemon->getUserPokemon()->getUserPokemonName() . 'をあなたのポケモンに登録しますか？' }}
				</div>
				<input type="hidden" name="favorite_id" value="{{ $favoriteMyPokemon->getId() }}">
				<div class="add_favorite_my_pokemon_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_add_favorite_my_pokemon">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>