@extends('common.templete')

@section('title', 'お知らせ編集')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">
			
			@component(
				'admin_edit_notice.form',
				[
					'notice' => $vm->notice
				]
			)@endcomponent

			@component('admin_edit_notice.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection