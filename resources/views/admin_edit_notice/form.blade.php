<section>
	<h2>お知らせ編集フォーム</h2>

	<form action="{{ route('edit_notice', ['noticeId' => $notice->getId()]) }}" method="post">

		@csrf

		<table>
			<tr>
				<th>本文</th>
				<td>
					<textarea 
						type="text" 
						name="notice_body" 
						required
					>{{ count($errors) > 0 ? old('notice_form') : $notice->getBody() }}</textarea>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div class="edit_notice_buttons">
			<button type="submit">編集</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
		
	</form>
</section>