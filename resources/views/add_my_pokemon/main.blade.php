@extends('common.templete')

@section('title', 'ポケモン新規登録')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'add_my_pokemon.form',
				[
					'items' => $vm->items,
					'pokemons' => $vm->pokemons
				]
			)@endcomponent

			@component('add_my_pokemon.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection