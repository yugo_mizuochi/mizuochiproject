<section>
	<h2>新規登録フォーム</h2>	

	<form action="{{ route('add_my_pokemon') }}" method="post">

		@csrf

		<table>
			<tr>
				<th>ポケモン</th>
				<td>
					<select name="pokemon_id" required>
						@foreach($pokemons as $pokemon)
							<option 
								value="{{ $pokemon->getId() }}"
								{{ $pokemon->getId() == old('pokemon_id') ? 'selected' : '' }}
							>
								{{ $pokemon->getName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン命名</th>
				<td>
					<input type="text" name="my_pokemon_name" value="{{ old('my_pokemon_name') }}" required>
				</td>
			</tr>
			<tr>
				<th>持ち物</th>
				<td>
					<select name="item_id" required>
						@foreach($items as $item)
							<option 
								value="{{ $item->getId() }}"
								{{ $item->getId() == old('item_id') ? 'selected' : '' }}
							>
								{{ $item->getName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>HP努力値</th>
				<td>
					<input type="number" name="effort_values[hp_value]" value="{{ old('effort_values.hp_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>攻撃努力値</th>
				<td>
					<input type="number" name="effort_values[attack_value]" value="{{ old('effort_values.attack_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>防御努力値</th>
				<td>
					<input type="number" name="effort_values[defense_value]" value="{{ old('effort_values.defense_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>特攻努力値</th>
				<td>
					<input type="number" name="effort_values[special_attack_value]" value="{{ old('effort_values.special_attack_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>特防努力値</th>
				<td>
					<input type="number" name="effort_values[special_defense_value]" value="{{ old('effort_values.special_defense_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>素早さ努力値</th>
				<td>
					<input type="number" name="effort_values[agility_value]" value="{{ old('effort_values.agility_value') }}" required>
				</td>
			</tr>
			<tr>
				<th>メモ</th>
				<td>
					<textarea type="text" name="my_pokemon_memo" required>{{ old('my_pokemon_memo') }}</textarea>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div id="add_mypokemon_buttons">
			<button type="submit">登録</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
	</form>
</section>