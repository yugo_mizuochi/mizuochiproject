<section>
	<h2>ユーザー新規登録フォーム</h2>

	<form action="{{ route('add_user') }}" method="post">

		@csrf

		<table>
			<tr>
				<th>名前</th>
				<td>
					<input type="text" name="user_name"  value="{{ old('user_name') }}" required>
				</td>
			</tr>
			<tr>
				<th>Eメール</th>
				<td>
					<input type="email" name="user_email" value="{{ old('user_email') }}" required>
				</td>
			</tr>
			<tr>
				<th>パスワード</th>
				<td>
					<input type="password" name="user_password" value="{{ old('user_password') }}" required>
				</td>
			</tr>
			<tr>
				<th>パスワード確認</th>
				<td>
					<input type="password" name="user_password_confirmation" value="{{ old('user_password_confirmation') }}" required>
				</td>
			</tr>
			<tr>
				<th>属性</th>
				<td>
					<select name="user_type">
						@foreach ($userTypes as $key => $userType)
							<option
								value="{{ $key }}" 
								{{ $key == old('user_type') ? 'selected' : '' }}
							>
								{{ $userType }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div id="add_user_buttons">
			<button type="submit">登録</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
		
	</form>
</section>