@extends('common.templete')

@section('title', 'ユーザー新規登録')

@include('common.admin_nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'admin_add_user.form',
				[
					'userTypes' => $vm->userTypes
				]
			)@endcomponent

			@component('admin_add_user.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection