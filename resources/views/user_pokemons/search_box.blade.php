<section>
	<h2>検索</h2>
	<form action="{{ route('user_pokemons') }}" method="get">
		<div class="search_box">
			<label>ポケモン名で検索</label>
			<select name="search_pokemon_id">
				@foreach($pokemons as $pokemon)
					@if($loop->first)
						<option value="0">全て</option>
					@endif
					<option 
						value="{{ $pokemon->getId() }}"
						{{ $pokemon->getId() == $searchId ? 'selected' : '' }}
					>
						{{ $pokemon->getName() }}
					</option>
				@endforeach
			</select>
		</div>
		<div id="search_button_box">
			<button>検索</button>
		</div>
	</form>
</section>