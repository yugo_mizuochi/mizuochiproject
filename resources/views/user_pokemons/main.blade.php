@extends('common.templete')

@section('title', 'ポケモンを探す')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">
			@component('common.flash_message')@endcomponent

			@component(
				'user_pokemons.search_box', 
				[
					'pokemons' => $vm->pokemons,
					'searchId' => $vm->searchId
				]
			)@endcomponent

			@component(
				'user_pokemons.user_pokemons_table', 
				[
					'userPokemons' => $vm->userPokemons,
					'myId' => $vm->myId
				]
			)@endcomponent

			@component('user_pokemons.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection