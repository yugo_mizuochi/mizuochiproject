<div class="release_modal_content">
	<div class="overlay">
		<div class="release_user_pokemon_modal">
			<form action="{{ route('release_favorite_user_pokemon') }}" method="post">
				@csrf
				<div class="release_modal_text">
					{{ $userPokemon->getUserPokemonName() . 'をお気に入り解除しますか？' }}
				</div>
				<input type="hidden" name="user_pokemon_id" value="{{ $userPokemon->getId() }}">
				<div class="release_user_pokemon_buttons">
					<button type="submit">OK</button>
					<button type="button" class="cancel_release_user_pokemon">キャンセル</button>
				</div>
			</form>
		</div>
	</div>
</div>