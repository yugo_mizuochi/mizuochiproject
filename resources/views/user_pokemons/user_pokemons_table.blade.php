<section>
	<h2>検索結果　{{ $userPokemons->getTotal() . '件' }}</h2>

	@foreach($userPokemons as $userPokemon)
		<table class="ta1">
			<thead>
				<tr>
					<th></th>
					<th>ポケモン名</th>
					<th>命名</th>
					<th>持ち物</th>
					<th>HP</th>
					<th>攻撃</th>
					<th>防御</th>
					<th>特攻</th>
					<th>特防</th>
					<th>素早さ</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<img src="{{ asset('img/sprites/' . $userPokemon->getPokemon()->getId() . 'MS.png') }}">
					</td>
					<td>{{ $userPokemon->getPokemon()->getName() }}</td>
					<td class="user_pokemon_name">{{ $userPokemon->getUserPokemonName() }}</td>
					<td>{{ $userPokemon->getItem()->getName() }}</td>
					<td>{{ $userPokemon->getHp() }}</td>
					<td>{{ $userPokemon->getAttack() }}</td>
					<td>{{ $userPokemon->getDefense() }}</td>
					<td>{{ $userPokemon->getSpecialAttack() }}</td>
					<td>{{ $userPokemon->getSpecialDefense() }}</td>
					<td>{{ $userPokemon->getAgility() }}</td>
					<td>
						<button class={{ $userPokemon->getModalButtonClass() }}>{{ $userPokemon->getFavoriteStatus() }}</button>
						@component('user_pokemons.' . $userPokemon->getModalName(), ['userPokemon' => $userPokemon])@endcomponent
					</td>
				</tr>
				<tr>
					<td>メモ</td>
					<td colspan="10">{{ $userPokemon->getMemo() }}</td>
				</tr>
			</tbody>
		</table>
	@endforeach
</section>

{{ $userPokemons->getLink() }}