<section>
	<h2>新規登録フォーム</h2>

	<form action="{{ route('edit_my_party', ['myPartyId' => $myParty->getId()]) }}" method="post">
		
		@csrf

		<table>
			<tr>
				<th>パーティー名</th>
				<td>
					<input 
						type="text" 
						name="my_party_name" 
						value="{{ count($errors) > 0 ? old('my_party_name') : $myParty->getName() }}" 
						required
					>
				</td>
			</tr>
			<tr>
				<th>ポケモン1</th>
				<td>
					<select name="my_pokemon_id[first_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getFirstMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id.first_my_pokemon_id') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン2</th>
				<td>
					<select name="my_pokemon_id[second_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getSecondMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id.second_my_pokemon_id') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン3</th>
				<td>
					<select name="my_pokemon_id[third_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getThirdMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id.third_my_pokemon_id') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン4</th>
				<td>
					<select name="my_pokemon_id[fourth_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getFourthMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id.fourth_my_pokemon_id') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン5</th>
				<td>
					<select name="my_pokemon_id[fifth_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getFifthMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id.fifth_my_pokemon_id') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>ポケモン6</th>
				<td>
					<select name="my_pokemon_id[sixth_my_pokemon_id]">
						@foreach($myPokemons as $myPokemon)
							<option
								value="{{ $myPokemon->getId() }}"
								{{ $myPokemon->getId() == $myParty->getSixthMyPokemon()->getId() || $myPokemon->getId() == old('my_pokemon_id[sixth_my_pokemon_id]') ? 'selected' : '' }}
							>
								{{ $myPokemon->getMyPokemonName() }}
							</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<th>メモ</th>
				<td>
					<textarea 
						type="text" 
						name="my_party_memo" 
						required
					>{{  count($errors) > 0 ? old('my_party_memo') : $myParty->getMemo() }}</textarea>
				</td>
			</tr>
		</table>

		@component('common.form_errors')@endcomponent

		<div id="edit_myparty_buttons">
			<button type="submit">編集</button>
			<button type="button" onclick="history.back()">キャンセル</button>
		</div>
	</form>
</section>