@extends('common.templete')

@section('title', 'パーティー編集')

@include('common.nav')

@include('common.footer')

@section('content')

	<div id="container">

		<div id="main">

			@component(
				'edit_my_party.form',
				[
					'myParty' => $vm->myParty,
					'myPokemons' => $vm->myPokemons
				]
			)@endcomponent

			@component('edit_my_party.asset')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection