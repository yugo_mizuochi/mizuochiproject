<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ItemsTableSeeder extends Seeder
{
	const ItemsMasterData = 'master_json/items.json';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = Storage::get(self::ItemsMasterData);
		$items = json_decode($file, true);

		foreach($items as $item)
		{
			DB::table('items')->insert([
				[
					'name' => $item['name']['japanese'],
					'created_at' => date('Y/m/d')
				]
			]);
		}
    }
}
