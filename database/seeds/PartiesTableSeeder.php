<?php

use Illuminate\Database\Seeder;
use App\Model\UserPokemon;
use App\Model\User;

class PartiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $userIds = [];
        foreach($users as $user) {
            $userIds[] = $user->id;
        }
        for ($i = 0; $i < 40; $i++)
        {
            $userId = $userIds[array_rand($userIds)];
        	$userPokemons = UserPokemon::where('user_id', $userId)->inRandomOrder()->take(6)->get();

        	DB::table('parties')->insert([
        		'user_id' => $userId,
                'name' => 'サンプルパーティー' . $i,
        		'first_user_pokemon_id' => $userPokemons[0]->id,
        		'second_user_pokemon_id' => $userPokemons[1]->id,
        		'third_user_pokemon_id' => $userPokemons[2]->id,
        		'fourth_user_pokemon_id' => $userPokemons[3]->id,
        		'fifth_user_pokemon_id' => $userPokemons[4]->id,
        		'sixth_user_pokemon_id' => $userPokemons[5]->id,
        		'memo' => 'サンプルテキスト',
        		'created_at' => date('Y/m/d')
        	]);
        }
    }
}
