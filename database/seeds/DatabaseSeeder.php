<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            NoticiesTableSeeder::class,
            ItemsTableSeeder::class,
            PokemonsTableSeeder::class,
            UserPokemonsTableSeeder::class,
            PartiesTableSeeder::class
        ]);
    }
}
