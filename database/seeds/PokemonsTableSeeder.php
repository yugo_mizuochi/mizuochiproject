<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PokemonsTableSeeder extends Seeder
{
	const pokemonMasterData = 'master_json/pokedex.json';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = Storage::get(self::pokemonMasterData);
		$pokemons = json_decode($file, true);

		foreach($pokemons as $pokemon)
		{
			DB::table('pokemons')->insert([
				[
					'id' => $pokemon['id'],
					'name' => $pokemon['name']['japanese'],
					'created_at' => date('Y/m/d')
				]
			]);
		}
    }
}
