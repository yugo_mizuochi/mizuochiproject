<?php

use Illuminate\Database\Seeder;

class NoticiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('noticies')->insert([
        	[
        		'body' => '赤、緑に対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => '青、黄に対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => '金、銀に対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'クリスタルに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ルビー、サファイアに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'エメラルドに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ファイアーレッド、リーフグリーンに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ダイヤモンド、パールに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'プラチナに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ハートゴールド、ソールシルバーに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ブラック、ホワイトに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ブラック２、ホワイト２に対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'X、Yに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'オメガルビー、アルファサファイアに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'サン、ムーンに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ウルトラサン、ウルトラムーンに対応しました。',
                'created_at' => date('Y/m/d')
        	],
        	[
        		'body' => 'ソード、シールドに対応しました。',
                'created_at' => date('Y/m/d')
        	],
    	]);
    }
}
