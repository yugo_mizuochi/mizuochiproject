<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Enums\UserType;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
	        [
	        	'name' => 'Mizuochi Yugo',
	        	'email' => 'elric.mizuochi@i.softbank.jp',
	        	'password' => Hash::make('Mizuochi5525'),
	        	'user_type' => UserType::ADMIN,
	        	'remember_token' => null,
	        	'created_at' => date('Y/m/d')
	        ],
	        [
	        	'name' => 'テスト太郎',
	        	'email' => 'test@co.jp',
	        	'password' => Hash::make('password'),
	        	'user_type' => UserType::GENERAL,
	        	'remember_token' => null,
	        	'created_at' => date('Y/m/d')	
	        ],
        ]);
    }
}
