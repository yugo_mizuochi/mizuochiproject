<?php

use Illuminate\Database\Seeder;
use App\Model\Pokemon;
use App\Model\User;
use App\Model\Item;

class UserPokemonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $items = Item::all();
        $pokemons = Pokemon::all();
        $userIds = [];
        $itemIds = [];
        $pokemonIds = [];
        foreach($users as $user) {
            $userIds[] = $user->id;
        }
        foreach($items as $item) {
            $itemIds[] = $item->id;
        }
        foreach($pokemons as $pokemon) {
            $pokemonIds[] = $pokemon->id;
        }

        for ($i = 1; $i <= 100; $i++)
        {
            $pokemon = Pokemon::find($pokemonIds[array_rand($pokemonIds)]);
            DB::table('user_pokemons')->insert([
                'user_id' => $userIds[array_rand($userIds)],
                'pokemon_id' => $pokemon->id,
                'pokemon_name' => 'サンプル' . $pokemon->name,
                'item_id' => $itemIds[array_rand($itemIds)],
                'hp' => 4,
                'attack' => 252,
                'defense' => 0,
                'special_attack' => 0,
                'special_defense' => 0,
                'agility' => 252,
                'memo' => 'サンプルテキスト',
                'created_at' => date('Y/m/d')
            ]);
        }
    }
}
