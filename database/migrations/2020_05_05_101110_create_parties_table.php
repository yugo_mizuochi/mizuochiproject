<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 255);
            $table->integer('first_user_pokemon_id')->unsigned();
            $table->integer('second_user_pokemon_id')->unsigned();
            $table->integer('third_user_pokemon_id')->unsigned();
            $table->integer('fourth_user_pokemon_id')->unsigned();
            $table->integer('fifth_user_pokemon_id')->unsigned();
            $table->integer('sixth_user_pokemon_id')->unsigned();
            $table->string('memo', 5000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parties');
    }
}
