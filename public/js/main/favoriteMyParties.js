$(function() {
	$('.add_favorite_my_party_modal_button').on('click', function() {
		let index = $('.add_favorite_my_party_modal_button').index(this);
		let addFavoriteMyPartyModal = $('.add_favorite_my_party_modal_content').eq(index);
		addFavoriteMyPartyModal.fadeIn('slow');
	});

	$('.cancel_add_favorite_my_party').on('click', function() {
		let index = $('.cancel_add_favorite_my_party').index(this);
		let addFavoriteMyPartyModal = $('.add_favorite_my_party_modal_content').eq(index);
		addFavoriteMyPartyModal.fadeOut();
	});

	$('.release_favorite_my_party_modal_button').on('click', function() {
		let index = $('.release_favorite_my_party_modal_button').index(this);
		let releaseFavoriteMyPartyModal = $('.release_favorite_my_party_modal_content').eq(index);
		releaseFavoriteMyPartyModal.fadeIn('slow');
	});

	$('.cancel_release_favorite_my_party').on('click', function() {
		let index = $('.cancel_release_favorite_my_party').index(this);
		let releaseFavoriteMyPartyModal = $('.release_favorite_my_party_modal_content').eq(index);
		releaseFavoriteMyPartyModal.fadeOut();
	});
});