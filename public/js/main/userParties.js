$(function() {
	$('.favorite_user_party_modal_button').on('click', function() {
		let index = $('.favorite_user_party_modal_button').index(this);
		let favoriteModal = $('.favorite_user_party_modal_content').eq(index);
		favoriteModal.fadeIn('slow');
	});

	$('.cancel_favorite_user_party').on('click', function() {
		let index = $('.cancel_favorite_user_party').index(this);
		let favoriteModal = $('.favorite_user_party_modal_content').eq(index);
		favoriteModal.fadeOut();
	});

	$('.release_user_party_modal_button').on('click', function() {
		let index = $('.release_user_party_modal_button').index(this);
		let releaseModal = $('.release_user_party_modal_content').eq(index);
		releaseModal.fadeIn('slow');
	});

	$('.cancel_release_user_party').on('click', function() {
		let index = $('.cancel_release_user_party').index(this);
		let releaseModal = $('.release_user_party_modal_content').eq(index);
		releaseModal.fadeOut();
	});
});