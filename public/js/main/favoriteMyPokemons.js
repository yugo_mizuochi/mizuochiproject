$(function() {
	$('.add_favorite_my_pokemon_modal_button').on('click', function() {
		let index = $('.add_favorite_my_pokemon_modal_button').index(this);
		let addFavoriteMyPokemonModal = $('.add_favorite_my_pokemon_modal_content').eq(index);
		addFavoriteMyPokemonModal.fadeIn('slow');
	});

	$('.cancel_add_favorite_my_pokemon').on('click', function() {
		let index = $('.cancel_add_favorite_my_pokemon').index(this);
		let addFavoriteMyPokemonModal = $('.add_favorite_my_pokemon_modal_content').eq(index);
		addFavoriteMyPokemonModal.fadeOut();
	});

	$('.release_favorite_my_pokemon_modal_button').on('click', function() {
		let index = $('.release_favorite_my_pokemon_modal_button').index(this);
		let releaseFavoriteMyPokemonModal = $('.release_favorite_my_pokemon_modal_content').eq(index);
		releaseFavoriteMyPokemonModal.fadeIn('slow');
	});

	$('.cancel_release_favorite_my_pokemon').on('click', function() {
		let index = $('.cancel_release_favorite_my_pokemon').index(this);
		let releaseFavoriteMyPokemonModal = $('.release_favorite_my_pokemon_modal_content').eq(index);
		releaseFavoriteMyPokemonModal.fadeOut();
	});
});