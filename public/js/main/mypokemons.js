$(function() {
	$('.delete_my_pokemon_modal_button').on('click', function() {
		let index = $('.delete_my_pokemon_modal_button').index(this);
		let deleteModal = $('.delete_modal_content').eq(index);
		deleteModal.fadeIn('slow');
	});

	$('.cancel_delete_my_pokemon').on('click', function() {
		let index = $('.cancel_delete_my_pokemon').index(this);
		let deleteModal = $('.delete_modal_content').eq(index);
		deleteModal.fadeOut();
	});
});