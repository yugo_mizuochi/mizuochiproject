$(function() {
	$('.delete_my_party_modal_button').on('click', function() {
		let index = $('.delete_my_party_modal_button').index(this);
		let deleteMyPartyModal = $('.delete_my_party_modal_content').eq(index);
		deleteMyPartyModal.fadeIn('slow');
	});

	$('.cancel_delete_my_party').on('click', function() {
		let index = $('.cancel_delete_my_party').index(this);
		let deleteMyPartyModal = $('.delete_my_party_modal_content').eq(index);
		deleteMyPartyModal.fadeOut();
	});
});