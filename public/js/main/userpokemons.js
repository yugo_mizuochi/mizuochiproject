$(function() {
	$('.favorite_user_pokemon_modal_button').on('click', function() {
		let index = $('.favorite_user_pokemon_modal_button').index(this);
		let favoriteModal = $('.favorite_modal_content').eq(index);
		favoriteModal.fadeIn('slow');
	});

	$('.cancel_favorite_user_pokemon').on('click', function() {
		let index = $('.cancel_favorite_user_pokemon').index(this);
		let favoriteModal = $('.favorite_modal_content').eq(index);
		favoriteModal.fadeOut();
	});

	$('.release_user_pokemon_modal_button').on('click', function() {
		let index = $('.release_user_pokemon_modal_button').index(this);
		let releaseModal = $('.release_modal_content').eq(index);
		releaseModal.fadeIn('slow');
	});

	$('.cancel_release_user_pokemon').on('click', function() {
		let index = $('.cancel_release_user_pokemon').index(this);
		let releaseModal = $('.release_modal_content').eq(index);
		releaseModal.fadeOut();
	});
});