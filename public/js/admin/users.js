$(function() {
	$('.delete_user_modal_button').on('click', function() {
		let index = $('.delete_user_modal_button').index(this);
		let deleteUserModal = $('.delete_user_modal_content').eq(index);
		deleteUserModal.fadeIn('slow');
	});

	$('.cancel_delete_user').on('click', function() {
		let index = $('.cancel_delete_user').index(this);
		let deleteUserModal = $('.delete_user_modal_content').eq(index);
		deleteUserModal.fadeOut();
	});
});