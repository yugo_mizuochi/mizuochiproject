$(function() {
	$('.delete_notice_modal_button').on('click', function() {
		let index = $('.delete_notice_modal_button').index(this);
		let deleteNoticeModal = $('.delete_notice_modal_content').eq(index);
		deleteNoticeModal.fadeIn('slow');
	});

	$('.cancel_delete_notice').on('click', function() {
		let index = $('.cancel_delete_notice').index(this);
		let deleteNoticeModal = $('.delete_notice_modal_content').eq(index);
		deleteNoticeModal.fadeOut();
	});
});