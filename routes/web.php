<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
	Route::get('/', function () {
	    return view('auth.login');
	});

	Auth::routes();

	Route::get('/home', 'Home\HomeController@showHome')->name('home');

	Route::get('/my_pokemons', 'MyPokemons\MyPokemonsController@showMyPokemons')->name('my_pokemons');

	Route::get('/add_my_pokemon/form', 'MyPokemons\MyPokemonsController@showAddMyPokemonForm')->name('add_my_pokemon.form');

	Route::post('/add_my_pokemon', 'MyPokemons\MyPokemonsController@addMyPokemon')->name('add_my_pokemon');

	Route::get('/edit_my_pokemon/form/{myPokemonId}', 'MyPokemons\MyPokemonsController@showEditMyPokemonFrom')
	->where('myPokemonId', '[0-9]+')
	->name('edit_my_pokemon.form');

	Route::post('/edit_my_pokemon/{myPokemonId}', 'MyPokemons\MyPokemonsController@editMyPokemon')
	->where('myPokemonId', '[0-9]+')
	->name('edit_my_pokemon');

	Route::post('/delete_my_pokemon', 'MyPokemons\MyPokemonsController@deleteMyPokemon')
	->name('delete_my_pokemon');

	Route::get('/search/pokemons', 'UserPokemons\UserPokemonsController@showUserPokemons')
	->name('user_pokemons');

	Route::post('/add_favorite_user_pokemon', 'UserPokemons\UserPokemonsController@addFavoriteUserPokemon')
	->name('add_favorite_user_pokemon');

	Route::post('/release_favorite_user_pokemon', 'UserPokemons\UserPokemonsController@releaseFavoriteUserPokemon')
	->name('release_favorite_user_pokemon');

	Route::get('/favorite_my_pokemons', 'FavoriteMyPokemons\FavoriteMyPokemonsController@showFavoriteMyPokemons')
	->name('favorite_my_pokemons');

	Route::post('/add_favorite_to_my_pokemon', 'FavoriteMyPokemons\FavoriteMyPokemonsController@addFavoriteToMyPokemon')
	->name('add_favorite_to_my_pokemon');

	Route::post('/release_favorite_my_pokemon', 'FavoriteMyPokemons\FavoriteMyPokemonsController@releaseFavoriteMyPokemon')
	->name('release_favorite_my_pokemon');

	Route::get('/my_parties', 'MyParties\MyPartiesController@showMyParties')
	->name('my_parties');

	Route::get('/add_my_party/form', 'MyParties\MyPartiesController@showAddMyPartyForm')
	->name('add_my_party_form');

	Route::post('/add_my_party', 'MyParties\MyPartiesController@addMyParty')
	->name('add_my_party');

	Route::get('/edit_my_party/form/{myPartyId}', 'MyParties\MyPartiesController@showEditMyPartyForm')
	->where('myPartyId', '[0-9]+')
	->name('edit_my_party_form');

	Route::post('/edit_my_party/{myPartyId}', 'MyParties\MyPartiesController@editMyParty')
	->where('myPartyId', '[0-9]+')
	->name('edit_my_party');

	Route::post('/delete_my_party', 'MyParties\MyPartiesController@deleteMyParty')
	->name('delete_my_party');

	Route::get('/search_parties', 'UserParties\UserPartiesController@showUserParties')
	->name('user_parties');

	Route::post('/add_favorite_user_party', 'UserParties\UserPartiesController@addFavoriteUserParty')
	->name('add_favorite_user_party');

	Route::post('/release_favorite_user_party', 'UserParties\UserPartiesController@releaseFavoriteUserParty')
	->name('release_favorite_user_party');

	Route::get('/favorite_my_parties', 'FavoriteMyParties\FavoriteMyPartiesController@showFavoriteMyParties')
	->name('favorite_my_parties');

	Route::post('/add_favorite_to_my_party', 'FavoriteMyParties\FavoriteMyPartiesController@addFavoriteToMyParty')
	->name('add_favorite_to_my_party');

	Route::post('/release_favorite_my_party', 'FavoriteMyParties\FavoriteMyPartiesController@releaseFavoriteMyParty')
	->name('release_favorite_my_party');

	Route::get('/noticies', 'AdminNoticies\AdminNoticiesController@showNoticies')
	->name('admin_noticies');

	Route::get('/add_notice/form', 'AdminNoticies\AdminNoticiesController@showAddNoticeForm')
	->name('add_notice_form');

	Route::post('/add_notice', 'AdminNoticies\AdminNoticiesController@addNotice')
	->name('add_notice');

	Route::get('/edit_notice/form/{noticeId}', 'AdminNoticies\AdminNoticiesController@showEditNoticeForm')
	->where('noticeId', '[0-9]+')
	->name('edit_notice_form');

	Route::post('/edit_notice/{noticeId}', 'AdminNoticies\AdminNoticiesController@editNotice')
	->where('noticeId', '[0-9]+')
	->name('edit_notice');

	Route::post('/delete_notice', 'AdminNoticies\AdminNoticiesController@deleteNotice')
	->name('delete_notice');

	Route::get('/users', 'AdminUsers\AdminUsersController@showUsers')
	->name('admin_users');

	Route::get('/add_user/form', 'AdminUsers\AdminUsersController@showAddUserForm')
	->name('add_user_form');

	Route::post('/add_user', 'AdminUsers\AdminUsersController@addUser')
	->name('add_user');

	Route::get('/edit_user_form/{userId}', 'AdminUsers\AdminUsersController@showEditUserForm')
	->where('userId', '[0-9]+')
	->name('edit_user_form');

	Route::post('/edit_user/{userId}', 'AdminUsers\AdminUsersController@editUser')
	->where('userId', '[0-9]+')
	->name('edit_user');

	Route::post('/delete_user', 'AdminUsers\AdminUsersController@deleteUser')
	->name('delete_user');

	Route::get('/logout', 'Auth\LogoutController@logout')
	->name('logout');
});
