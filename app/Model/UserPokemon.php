<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPokemon extends Model
{
	use SoftDeletes;
	
	protected $table = 'user_pokemons';
	
	protected $fillable = [
		'user_id',
		'pokemon_id',
		'pokemon_name',
		'item_id',
		'hp',
		'attack',
		'defense',
		'special_attack',
		'special_defense',
		'agility',
		'memo'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	/**
	 * [user description]
	 * @return [type] [description]
	 */
	public function user()
	{
		return $this->belongsTo('App\Model\User');
	}

	/**
	 * [pokemon description]
	 * @return [type] [description]
	 */
	public function pokemon()
	{
		return $this->belongsTo('App\Model\Pokemon');
	}

	/**
	 * [item description]
	 * @return [type] [description]
	 */
	public function item()
	{
		return $this->belongsTo('App\Model\Item');
	}

	/**
	 * [favorites description]
	 * @return [type] [description]
	 */
	public function favorites()
	{
		return $this->hasMany('App\Model\Favorite');
	}

	/**
	 * [firstUserPokemons description]
	 * @return [type] [description]
	 */
	public function firstUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'first_user_pokemon_id');
	}

	/**
	 * [secondUserPokemons description]
	 * @return [type] [description]
	 */
	public function secondUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'second_user_pokemon_id');
	}

	/**
	 * [thirdUserPokemons description]
	 * @return [type] [description]
	 */
	public function thirdUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'third_user_pokemon_id');
	}

	/**
	 * [fourthUserPokemons description]
	 * @return [type] [description]
	 */
	public function fourthUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'fourth_user_pokemon_id');
	}

	/**
	 * [fifthUserPokemons description]
	 * @return [type] [description]
	 */
	public function fifthUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'fifth_user_pokemon_id');
	}

	/**
	 * [sixthUserPokemons description]
	 * @return [type] [description]
	 */
	public function sixthUserPokemons()
	{
		return $this->hasMany('App\Model\Party', 'sixth_user_pokemon_id');
	}
}