<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
	use SoftDeletes;

	protected $table = 'parties';

	protected $fillable = [
		'user_id',
		'name',
		'first_user_pokemon_id',
		'second_user_pokemon_id',
		'third_user_pokemon_id',
		'fourth_user_pokemon_id',
		'fifth_user_pokemon_id',
		'sixth_user_pokemon_id',
		'memo'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	/**
	 * [user description]
	 * @return [type] [description]
	 */
	public function user()
	{
		return $this->belongsTo('App\Model\User');
	}

	/**
	 * [firstUserPokemon description]
	 * @return [type] [description]
	 */
	public function firstUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [secondUserPokemon description]
	 * @return [type] [description]
	 */
	public function secondUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [thirdUserPokemon description]
	 * @return [type] [description]
	 */
	public function thirdUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [fourthUserPokemon description]
	 * @return [type] [description]
	 */
	public function fourthUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [fifthUserPokemon description]
	 * @return [type] [description]
	 */
	public function fifthUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [sixthUserPokemon description]
	 * @return [type] [description]
	 */
	public function sixthUserPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon');
	}

	/**
	 * [favoriteParties description]
	 * @return [type] [description]
	 */
	public function favoriteParties()
	{
		return $this->hasMany('App\Model\FavoriteParty', 'user_party_id');
	}
}