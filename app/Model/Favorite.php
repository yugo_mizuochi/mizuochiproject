<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
	protected $table = 'favorites';

	protected $fillable = [
		'user_id',
		'user_pokemon_id'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * [user description]
	 * @return [type] [description]
	 */
	public function user()
	{
		return $this->belongsTo('App\Model\User');
	}

	/**
	 * [userPokemon description]
	 * @return [type] [description]
	 */
	public function userPokemon()
	{
		return $this->belongsTo('App\Model\UserPokemon')->withTrashed();
	}
}