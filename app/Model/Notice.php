<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
	protected $table = 'noticies';

	protected $fillable = [
		'notice'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];
}