<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Enums\UserType;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * [$enumCasts description]
     * @var [type]
     */
    protected $enumCasts = [
        'user_type' => UserType::class
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * [$dates description]
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * [userPokemons description]
     * @return [type] [description]
     */
    public function userPokemons()
    {
        return $this->hasMany('App\Model\UserPokemon');
    }

    /**
     * [favorites description]
     * @return [type] [description]
     */
    public function favorites()
    {
        return $this->hasMany('App\Model\Favorite');
    }

    /**
     * [parties description]
     * @return [type] [description]
     */
    public function parties()
    {
        return $this->hasMany('App\Model\Party');
    }

    /**
     * [favoriteParties description]
     * @return [type] [description]
     */
    public function favoriteParties()
    {
        return $this->hasMany('App\Model\FavoriteParty');
    }
}
