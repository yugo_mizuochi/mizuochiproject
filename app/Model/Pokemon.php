<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
	protected $table = 'pokemons';
	
	protected $fillable = [
		'name'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * [userPokemons description]
	 * @return [type] [description]
	 */
	public function userPokemons()
	{
		return $this->hasMany('App\Model\UserPokemon');
	}
}