<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	protected $table = 'items';
	
	protected $fillable = [
		'name'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * [userPokemons description]
	 * @return [type] [description]
	 */
	public function userPokemons()
	{
		return $this->hasMany('App\Model\UserPokemon');
	}
}