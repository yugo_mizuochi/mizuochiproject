<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FavoriteParty extends Model
{
	protected $table = 'favorite_parties';

	protected $fillable = [
		'user_id',
		'user_party_id'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * [user description]
	 * @return [type] [description]
	 */
	public function user()
	{
		return $this->belongsTo('App\Model\User');
	}

	/**
	 * [party description]
	 * @return [type] [description]
	 */
	public function party()
	{
		return $this->belongsTo('App\Model\Party', 'user_party_id')->withTrashed();
	}
}