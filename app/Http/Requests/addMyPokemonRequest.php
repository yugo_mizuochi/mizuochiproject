<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SumEffortValuesRule;

class addMyPokemonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pokemon_id' => 'required|exists:pokemons,id',
            'my_pokemon_name' => 'required|max:20',
            'item_id' => 'required|exists:items,id',
            'effort_values' => [
                new SumEffortValuesRule()
            ],
            'effort_values.hp_value' => 'required|integer|between:0,252',
            'effort_values.attack_value' => 'required|integer|between:0,252',
            'effort_values.defense_value' => 'required|integer|between:0,252',
            'effort_values.special_attack_value' => 'required|integer|between:0,252',
            'effort_values.special_defense_value' => 'required|integer|between:0,252',
            'effort_values.agility_value' => 'required|integer|between:0,252',
            'my_pokemon_memo' => 'required|max:5000'
        ];
    }
}
