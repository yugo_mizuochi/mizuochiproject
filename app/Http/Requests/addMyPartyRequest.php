<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PokemonComparison;
use App\Rules\ItemComparison;

class addMyPartyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'my_party_name' => 'required|max:20',
            'my_pokemon_id' => [
                'required', 
                'array', 
                new PokemonComparison($this->request->all()), 
                new ItemComparison($this->request->all())
            ],
            'my_pokemon_id.*' => 'required|exists:user_pokemons,id',
            'my_party_memo' => 'required|max:5000'
        ];
    }
}
