<?php

namespace App\Http\Controllers\MyParties;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UseCases\MyParty\MyPartyUseCase;
use App\Http\ViewModels\showMyPartiesViewModel;
use App\Http\ViewModels\showAddMyPartyFormViewModel;
use App\Http\ViewModels\showEditMyPartyFormViewModel;
use App\Http\Requests\addMyPartyRequest;
use App\Http\Requests\editMyPartyRequest;
use App\Model\Party;

class MyPartiesController extends Controller
{
	/** @var MyPartyUseCase $myPartyUseCase */
	private $myPartyUseCase;

	/**
	 * @param MyPartyUseCase $myPartyUseCase
	 */
	public function __construct(MyPartyUseCase $myPartyUseCase)
	{
		$this->myPartyUseCase = $myPartyUseCase;
		$this->middleware('auth');
	}

	/**
	 * マイパーティ一覧表示
	 *
	 * @return view
	 */
	public function showMyParties()
	{
		$myParties = $this->myPartyUseCase->getMyParties((int) Auth::id());
		$vm = (new showMyPartiesViewModel([
			'myParties' => $myParties
		]))->render();

		return view(
			'my_parties.main',
			compact('vm')
		);
	}

	/**
	 * マイパーティ新規登録画面表示
	 *
	 * @return view
	 */
	public function showAddMyPartyForm()
	{
		$myPokemons = $this->myPartyUseCase->getMyPokemons((int) Auth::id());
		$vm = (new showAddMyPartyFormViewModel([
			'myPokemons' => $myPokemons
		]))->render();

		return view(
			'add_my_party.main',
			compact('vm')
		);
	}

	/**
	 * マイパーティ新規登録
	 *
	 * @param addMyPartyRequest $request
	 * @return redirect
	 */
	public function addMyParty(addMyPartyRequest $request)
	{
		$addMyPartyMessage = $this->myPartyUseCase->addMyParty($request, Auth::id());

		return redirect('/my_parties')->with('flash_message', $addMyPartyMessage);
	}


	/**
	 * マイパーティ編集画面表示
	 *
	 * @param integer $myPartyId
	 * @return view
	 */
	public function showEditMyPartyForm(int $myPartyId)
	{
		$myPokemons = $this->myPartyUseCase->getMyPokemons((int) Auth::id());
		$myParty = $this->myPartyUseCase->getMyParty((int) $myPartyId);
		$vm = (new showEditMyPartyFormViewModel([
			'myParty' => $myParty,
			'myPokemons' => $myPokemons
		]))->render();

		return view(
			'edit_my_party.main',
			compact('vm')
		);
	}

	/**
	 * マイパーティ編集
	 *
	 * @param editMyPartyRequest $request
	 * @param integer $myPartyId
	 * @return redirect
	 */
	public function editMyParty(editMyPartyRequest $request, int $myPartyId)
	{
		$editMyPartyMessage = $this->myPartyUseCase->editMyParty($request, (int) $myPartyId, (int) Auth::id());

		return redirect('/my_parties')->with('flash_message', $editMyPartyMessage);
	}

	/**
	 * マイパーティ削除
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function deleteMyParty(Request $request)
	{
		$deleteMyPartyMessage = $this->myPartyUseCase->deleteMyParty($request);

		return redirect('/my_parties')->with('flash_message', $deleteMyPartyMessage);
	}
}