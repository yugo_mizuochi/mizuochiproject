<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\UseCases\Notice\NoticeUseCase;
use App\Http\ViewModels\HomeViewModel;

class HomeController extends Controller
{
	/** @var noticeUseCase $noticeUseCase */
	private $noticeUseCase;

	/**
	 * @param NoticeUseCase $noticeUseCase
	 */
	public function __construct(NoticeUseCase $noticeUseCase)
	{
		$this->middleware('auth');
		$this->noticeUseCase = $noticeUseCase;
	}

	/**
	 * ホーム画面
	 *
	 * @return view
	 */
	public function showHome()
	{
		$noticies = $this->noticeUseCase->getNoticies();
		$vm = (new HomeViewModel([
			'noticies' => $noticies
		]))->render();

		return view(
			'home.main', 
			compact('vm')
		);
	}
}