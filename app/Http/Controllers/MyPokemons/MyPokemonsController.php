<?php

namespace App\Http\Controllers\MyPokemons;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\UseCases\MyPokemon\MyPokemonUseCase;
use App\Http\Requests\addMyPokemonRequest;
use App\Http\Requests\editMyPokemonRequest;
use App\Http\Requests\deleteMyPokemonRequest;
use App\Http\ViewModels\MyPokemonsViewModel;
use App\Http\ViewModels\AddMyPokemonFormViewModel;
use App\Http\ViewModels\EditMyPokemonFormViewModel;

class MyPokemonsController extends Controller
{
	/** @var MyPokemonUseCase $myPokemonUseCase */
	private $myPokemonUseCase;

	/**
	 * @param MyPokemonUseCase $myPokemonUseCase
	 */
	public function __construct(MyPokemonUseCase $myPokemonUseCase)
	{
		$this->myPokemonUseCase = $myPokemonUseCase;
		$this->middleware('auth');
	}

	/**
	 * マイポケモン一覧表示
	 *
	 * @return view
	 */
	public function showMyPokemons()
	{
		$myPokemons = $this->myPokemonUseCase->getMyPokemons((int) Auth::id());
		$vm = (new MyPokemonsViewModel([
			'myPokemons' => $myPokemons
		]))->render();

		return view(
			'mypokemons.main', 
			compact('vm')
		);
	}

	/**
	 * マイポケモン新規登録フォーム表示
	 *
	 * @return object
	 */
	public function showAddMyPokemonForm()
	{
		$pokemons = $this->myPokemonUseCase->getPokemons();
		$items = $this->myPokemonUseCase->getItems();
		$vm = (new AddMyPokemonFormViewModel([
			'pokemons' => $pokemons,
			'items' => $items
		]))->render();

		return view(
			'add_my_pokemon.main', 
			compact('vm')
		);
	}

	/**
	 * マイポケモン新規登録
	 *
	 * @param addMyPokemonRequest $request
	 * @return redirect
	 */
	public function addMyPokemon(addMyPokemonRequest $request)
	{
		$addMyPokemonMessage = $this->myPokemonUseCase->addMyPokemon((int) Auth::id(), $request);

		return redirect('/my_pokemons')->with('flash_message', $addMyPokemonMessage);
	}

	/**
	 * マイポケモン編集フォーム表示
	 *
	 * @param integer $myPokemonId
	 * @return view
	 */
	public function showEditMyPokemonFrom(int $myPokemonId)
	{
		$myPokemon = $this->myPokemonUseCase->getMyPokemon((int) $myPokemonId);
		$pokemons = $this->myPokemonUseCase->getPokemons();
		$items = $this->myPokemonUseCase->getItems();
		$vm = (new EditMyPokemonFormViewModel([
			'myPokemon' => $myPokemon,
			'pokemons' => $pokemons,
			'items' => $items
		]))->render();

		return view(
			'edit_my_pokemon.main',
			compact('vm')
		);
	}

	/**
	 * マイポケモン編集
	 *
	 * @param editMyPokemonRequest $request
	 * @param integer $myPokemonId
	 * @return redirect
	 */
	public function editMyPokemon(editMyPokemonRequest $request, int $myPokemonId)
	{
		$editMyPokemonMessage = $this->myPokemonUseCase->editMyPokemon($request, (int) $myPokemonId, (int) Auth::id());

		return redirect('/my_pokemons')->with('flash_message', $editMyPokemonMessage);
	}

	/**
	 * マイポケモン削除
	 *
	 * @param deleteMyPokemonRequest $request
	 * @return string
	 */
	public function deleteMyPokemon(deleteMyPokemonRequest $request)
	{
		$deleteMyPokemonMessage = $this->myPokemonUseCase->deleteMyPokemon($request);

		return redirect('/my_pokemons')->with('flash_message', $deleteMyPokemonMessage);
	}
}