<?php

namespace App\Http\Controllers\UserPokemons;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UseCases\UserPokemon\UserPokemonUseCase;
use App\Http\ViewModels\showUserPokemonsViewModel;

class UserPokemonsController extends Controller
{
	/** @var UserPokemonUseCase $userPokemonUseCase */
	private $userPokemonUseCase;

	/**
	 * @param UserPokemonUseCase $userPokemonUseCase
	 */
	public function __construct(UserPokemonUseCase $userPokemonUseCase)
	{
		$this->userPokemonUseCase = $userPokemonUseCase;
		$this->middleware('auth');
	}

	/**
	 * ポケモン検索画面
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function showUserPokemons(Request $request)
	{
		$searchId = $this->userPokemonUseCase->getSearchId($request);
		$userPokemons = $this->userPokemonUseCase->getUserPokemons($searchId, (int) Auth::id());
		$pokemons = $this->userPokemonUseCase->getPokemons();

		$vm = (new showUserPokemonsViewModel([
			'userPokemons' => $userPokemons,
			'pokemons' => $pokemons,
			'searchId' => $searchId,
			'myId' => Auth::id()
		]))->render();

		return view('user_pokemons.main', compact('vm'));
	}

	/**
	 * ポケモンをお気に入り追加する
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function addFavoriteUserPokemon(Request $request)
	{
		$addFavoriteUserPokemonMessage = $this->userPokemonUseCase->addFavoriteUserPokemon($request, Auth::id());

		return redirect('/search/pokemons')->with('flash_message', $addFavoriteUserPokemonMessage);
	}

	/**
	 * ポケモンをお気に入り解除する
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function releaseFavoriteUserPokemon(Request $request)
	{
		$addReleaseUserPokemonMessage = $this->userPokemonUseCase->releaseFavoriteUserPokemon($request, Auth::id());

		return redirect('/search/pokemons')->with('flash_message', $addReleaseUserPokemonMessage);
	}
}