<?php

namespace App\Http\Controllers\FavoriteMyPokemons;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UseCases\FavoriteMyPokemon\FavoriteMyPokemonUseCase;
use App\Http\ViewModels\showFavoriteMyPokemonsViewModel;

class FavoriteMyPokemonsController extends Controller
{
	/** @var FavoriteMyPokemonUseCase $favoriteMyPokemonUseCase */
	private $favoriteMyPokemonUseCase;

	/**
	 * @param FavoriteMyPokemonUseCase $favoriteMyPokemonUseCase
	 */
	public function __construct(FavoriteMyPokemonUseCase $favoriteMyPokemonUseCase)
	{
		$this->favoriteMyPokemonUseCase = $favoriteMyPokemonUseCase;
		$this->middleware('auth');
	}

	/**
	 * お気に入りポケモン一覧
	 *
	 * @return view
	 */
	public function showFavoriteMyPokemons()
	{
		$favoriteMyPokemons = $this->favoriteMyPokemonUseCase->getFavoriteMyPokemons(Auth::id());
		$vm = (new showFavoriteMyPokemonsViewModel([
			'favoriteMyPokemons' => $favoriteMyPokemons
		]))->render();

		return view(
			'favorite_my_pokemons.main',
			compact('vm')
		);
	}

	/**
	 * お気に入りポケモンをマイポケモン登録
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function addFavoriteToMyPokemon(Request $request)
	{
		$addMyPokemonMessage = $this->favoriteMyPokemonUseCase->addFavoriteToMyPokemon($request, Auth::id());

		return redirect('/my_pokemons')->with('flash_message', $addMyPokemonMessage);
	}

	/**
	 * お気に入りポケモン解除
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function releaseFavoriteMyPokemon(Request $request)
	{
		$releaseFavoriteMyPokemonMessage = $this->favoriteMyPokemonUseCase->releaseFavoriteMyPokemon($request);

		return redirect('/favorite_my_pokemons')->with('flash_message', $releaseFavoriteMyPokemonMessage);
	}
}