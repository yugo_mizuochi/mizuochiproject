<?php

namespace App\Http\Controllers\AdminUsers;

use App\Http\Controllers\Controller;
use App\UseCases\AdminUser\AdminUserUseCase;
use App\Http\ViewModels\showUsersViewModel;
use App\Http\ViewModels\AddUserFormViewModel;
use App\Http\ViewModels\showEditUserFormViewModel;
use App\Http\Requests\addUserRequest;
use App\Http\Requests\editUserRequest;
use App\Http\Requests\deleteUserRequest;

class AdminUsersController extends Controller
{
	/** @var AdminUserUseCase $adminUserUseCase */
	private $adminUserUseCase;

	/**
	 * @param AdminUserUseCase $adminUserUseCase
	 */
	public function __construct(AdminUserUseCase $adminUserUseCase)
	{
		$this->adminUserUseCase = $adminUserUseCase;
		$this->middleware('auth');
	}

	/**
	 * ユーザー一覧表示
	 *
	 * @return view
	 */
	public function showUsers()
	{
		$users = $this->adminUserUseCase->getUsers();
		$vm = (new showUsersViewModel([
			'users' => $users
		]))->render();

		return view(
			'admin_users.main', 
			compact('vm')
		);
	}

	/**
	 * ユーザー新規登録画面表示
	 *
	 * @return view
	 */
	public function showAddUserForm()
	{
		$userTypes = $this->adminUserUseCase->getUserTypes();
		$vm = (new AddUserFormViewModel([
			'userTypes' => $userTypes
		]))->render();

		return view(
			'admin_add_user.main',
			compact('vm')
		);
	}

	/**
	 * ユーザー新規登録
	 *
	 * @param addUserRequest $request
	 * @return redirect
	 */
	public function addUser(addUserRequest $request)
	{
		$addUserMessage = $this->adminUserUseCase->addUser($request);

		return redirect('/users')->with('flash_message', $addUserMessage);
	}

	/**
	 * ユーザー編集画面表示
	 *
	 * @param integer $userId
	 * @return view
	 */
	public function showEditUserForm(int $userId)
	{
		$user = $this->adminUserUseCase->getUser($userId);
		$userTypes = $this->adminUserUseCase->getUserTypes();
		$vm = (new showEditUserFormViewModel([
			'user' => $user,
			'userTypes' => $userTypes
		]))->render();

		return view(
			'admin_edit_user.main',
			compact('vm')
		);
	}

	/**
	 * ユーザー編集
	 *
	 * @param editUserRequest $request
	 * @param integer $userId
	 * @return redirect
	 */
	public function editUser(editUserRequest $request, int $userId)
	{
		$editUserMessage = $this->adminUserUseCase->editUser($request, $userId);

		return redirect('/users')->with('flash_message', $editUserMessage);
	}

	/**
	 * ユーザー削除
	 *
	 * @param deleteUserRequest $request
	 * @return redirect
	 */
	public function deleteUser(deleteUserRequest $request)
	{
		$deleteUserMessage = $this->adminUserUseCase->deleteUser($request);

		return redirect('/users')->with('flash_message', $deleteUserMessage);
	}
}