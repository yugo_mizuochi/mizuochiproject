<?php

namespace App\Http\Controllers\AdminNoticies;

use App\Http\Controllers\Controller;
use App\UseCases\AdminNotice\AdminNoticeUseCase;
use App\Http\ViewModels\showNoticiesViewModel;
use App\Http\ViewModels\showEditNoticeFormViewModel;
use App\Http\Requests\addNoticeRequest;
use App\Http\Requests\editNoticeRequest;
use Illuminate\Http\Request;

class AdminNoticiesController extends Controller
{
	/** @var AdminNoticeUseCase $adminNoticeUseCase */
	private $adminNoticeUseCase;

	/**
	 * @param AdminNoticeUseCase $adminNoticeUseCase
	 */
	public function __construct(AdminNoticeUseCase $adminNoticeUseCase)
	{
		$this->adminNoticeUseCase = $adminNoticeUseCase;
		$this->middleware('auth');
	}

	/**
	 * お知らせ一覧画面表示
	 *
	 * @return view
	 */
	public function showNoticies()
	{
		$noticies = $this->adminNoticeUseCase->getNotices();

		$vm = (new showNoticiesViewModel([
			'noticies' => $noticies
		]))->render();

		return view(
			'admin_noticies.main',
			compact('vm')
		);
	}

	/**
	 * お知らせ新規登録画面表示
	 *
	 * @return view
	 */
	public function showAddNoticeForm()
	{
		return view('admin_add_notice.main');
	}

	/**
	 * お知らせ新規登録
	 *
	 * @param addNoticeRequest $request
	 * @return redirect
	 */
	public function addNotice(addNoticeRequest $request)
	{
		$addNoticeMessage = $this->adminNoticeUseCase->addNotice($request);

		return redirect('/noticies')->with('flash_message', $addNoticeMessage);
	}

	/**
	 * お知らせ編集画面表示
	 *
	 * @param integer $noticeId
	 * @return view
	 */
	public function showEditNoticeForm(int $noticeId)
	{
		$notice = $this->adminNoticeUseCase->getNotice((int) $noticeId);
		$vm = (new showEditNoticeFormViewModel([
			'notice' => $notice
		]))->render();

		return view(
			'admin_edit_notice.main',
			compact('vm')
		);
	}

	/**
	 * お知らせ編集
	 *
	 * @param editNoticeRequest $request
	 * @param integer $noticeId
	 * @return redirect
	 */
	public function editNotice(editNoticeRequest $request, int $noticeId)
	{
		$editNoticeMessage = $this->adminNoticeUseCase->editNotice($request, (int) $noticeId);

		return redirect('/noticies')->with('flash_message', $editNoticeMessage);
	}

	/**
	 * お知らせ削除
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function deleteNotice(Request $request)
	{
		$deleteNoticeMessage = $this->adminNoticeUseCase->deleteNotice($request);

		return redirect('/noticies')->with('flash_message', $deleteNoticeMessage);
	}
}