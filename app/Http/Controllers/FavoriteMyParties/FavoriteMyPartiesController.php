<?php

namespace App\Http\Controllers\FavoriteMyParties;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UseCases\FavoriteMyParty\FavoriteMyPartyUseCase;
use App\Http\ViewModels\showFavoriteMyPartiesViewModel;

class FavoriteMyPartiesController extends Controller
{
	/** @var FavoriteMyPartyUseCase $favoriteMyPartyUseCase */
	private $favoriteMyPartyUseCase;

	/**
	 * @param FavoriteMyPartyUseCase $favoriteMyPartyUseCase
	 */
	public function __construct(FavoriteMyPartyUseCase $favoriteMyPartyUseCase)
	{
		$this->favoriteMyPartyUseCase = $favoriteMyPartyUseCase;
		$this->middleware('auth');
	}

	/**
	 * お気に入りパーティ一覧表示
	 *
	 * @return view
	 */
	public function showFavoriteMyParties()
	{
		$favoriteMyParties = $this->favoriteMyPartyUseCase->getFavoriteMyParties((int) Auth::id());
		$vm = (new showFavoriteMyPartiesViewModel([
			'favoriteMyParties' => $favoriteMyParties
		]))->render();

		return view(
			'favorite_my_parties.main',
			compact('vm')
		);
	}

	/**
	 * お気に入りパーティをマイパーティに登録
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function addFavoriteToMyParty(Request $request)
	{
		$addMyPartyMessage = $this->favoriteMyPartyUseCase->addFavoriteToMyParty($request, (int) Auth::id());

		return redirect('/favorite_my_parties')->with('flash_message', $addMyPartyMessage);
	}

	/**
	 * お気に入りパーティを解除
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function releaseFavoriteMyParty(Request $request)
	{
		$releaseFavoriteMyPartyMessage = $this->favoriteMyPartyUseCase->releaseFavoriteMyParty($request);

		return redirect('/favorite_my_parties')->with('flash_message', $releaseFavoriteMyPartyMessage);
	}
}