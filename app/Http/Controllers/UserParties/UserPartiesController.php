<?php

namespace App\Http\Controllers\UserParties;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UseCases\UserParty\UserPartyUseCase;
use App\Http\ViewModels\showUserPartiesViewModel;

class UserPartiesController extends Controller
{
	/** @var UserPartyUseCase $userPartyUseCase */
	private $userPartyUseCase;

	/**
	 * @param UserPartyUseCase $userPartyUseCase
	 */
	public function __construct(UserPartyUseCase $userPartyUseCase)
	{
		$this->userPartyUseCase = $userPartyUseCase;
		$this->middleware('auth');
	}

	/**
	 * パーティ検索画面表示
	 *
	 * @param Request $request
	 * @return view
	 */
	public function showUserParties(Request $request)
	{
		$searchId = $this->userPartyUseCase->getSearchId($request);
		$userParties = $this->userPartyUseCase->getUserParties((int) Auth::id(), $searchId);
		$pokemons = $this->userPartyUseCase->getPokemons();

		$vm = (new showUserPartiesViewModel([
			'pokemons' => $pokemons,
			'userParties' => $userParties,
			'searchId' => $searchId
		]))->render();

		return view(
			'user_parties.main',
			compact('vm')
		);
	}

	/**
	 * パーティお気に入り追加
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function addFavoriteUserParty(Request $request)
	{
		$favoriteUserPartyMessage = $this->userPartyUseCase->addFavoriteUserParty($request, (int) Auth::id());

		return redirect('/search_parties')->with('flash_message', $favoriteUserPartyMessage);
	}

	/**
	 * パーティお気に入り解除
	 *
	 * @param Request $request
	 * @return redirect
	 */
	public function releaseFavoriteUserParty(Request $request)
	{
		$releaseUserPartyMessage = $this->userPartyUseCase->releaseFavoriteUserParty($request, (int) Auth::id());

		return redirect('/search_parties')->with('flash_message', $releaseUserPartyMessage);
	}
}