<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * [logout description]
	 * @return [type] [description]
	 */
	public function logout()
	{
		Auth::logout();
		return redirect('/');
	}
}