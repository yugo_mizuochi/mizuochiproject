<?php
namespace App\Consts;

final class MessageConst
{
    const FAVORITED_PARTY_MESSAGE = 'をお気に入りのパーティに追加しました。';
    const RELEASED_PARTY_MESSAGE = 'をお気に入りのパーティから解除しました。';
    const FAVORITED_POKEMON_MESSAGE = 'をお気に入りのポケモンに追加しました。';
    const RELEASED_POKEMON_MESSAGE = 'をお気に入りのポケモンから解除しました。';
    const ADD_MY_POKEMON_MESSAGE = 'をあなたのポケモンに登録しました。';
    const EDIT_MESSAGE = 'を編集しました。';
    const DELETE_MY_POKEMON_MESSAGE = 'をあなたのポケモンから削除しました';
    const ADD_MY_PARTY_MESSAGE = 'をあなたのパーティに登録しました。';
    const DELETE_MY_PARTY_MESSAGE = 'をあなたのパーティから削除しました。';
    const ADD_NOTICE_MESSAGE = 'をお知らせに登録しました。';
    const DELETE_NOTICE_MESSAGE = 'をお知らせから削除しました。';
    const ADD_USER_MESSAGE = 'をユーザーに登録しました。';
    const DELETE_USER_MESSAGE = 'をユーザーから削除しました。';
}