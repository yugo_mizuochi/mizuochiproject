<?php
namespace App\Consts;

final class HtmlAttributeConst
{
    const FAVORITE_USER_POKEMON_MODAL_BUTTON_CLASS = 'favorite_user_pokemon_modal_button';
    const RELEASE_USER_POKEMON_MODAL_BUTTON_CLASS = 'release_user_pokemon_modal_button';
    const FAVORITE_USER_PARTY_MODAL_BUTTON_CLASS = 'favorite_user_party_modal_button';
    const RELEASE_USER_PARTY_MODAL_BUTTON_CLASS = 'release_user_party_modal_button';
}