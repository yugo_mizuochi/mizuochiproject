<?php
namespace App\Consts;

final class ModalNameConst
{
    const FAVORITE_MODAL_NAME = 'favorite_modal';
    const RELEASE_MODAL_NAME = 'release_modal';
}