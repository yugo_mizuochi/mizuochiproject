<?php
namespace App\Repositories\UserPokemon;

use App\Entities\Pokemon\PokemonList;
use App\Entities\UserPokemon\UserPokemonList;
use App\Entities\UserPokemon\UserPokemon;

interface UserPokemonRepositoryInterface
{
    /**
     * ポケモン検索結果取得
     *
     * @param integer $searchId
     * @param integer $myId
     * @return UserPokemonList
     */
    public function getUserPokemons(int $searchId, int $myId);


    /**
     * ポケモンマスタ取得
     *
     * @return PokemonList
     */
    public function getPokemons();


    /**
     * 検索対象マスタデータ取得
     *
     * @param integer $searchId
     * @return Pokemon
     */
    public function getSearch(int $searchId);

    /**
     * ポケモンお気に入り追加処理
     *
     * @param integer $userPokemonId
     * @param integer $myId
     * @return UserPokemon
     */
    public function addFavorite(int $userPokemonId, int $myId);

    /**
     * ポケモンお気に入り解除処理
     *
     * @param integer $userPokemonId
     * @param integer $myId
     * @return UserPokemon
     */
    public function releaseFavorite(int $userPokemonId, int $myId);
}