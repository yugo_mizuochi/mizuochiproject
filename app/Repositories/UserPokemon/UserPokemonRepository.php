<?php
namespace App\Repositories\UserPokemon;

use App\Entities\SetEntity;
use App\Consts\HtmlAttributeConst;
use App\Consts\ModalNameConst;
use App\Enums\FavoriteStatus;
use App\Entities\UserPokemon\UserPokemon;
use App\Entities\UserPokemon\UserPokemonList;
use App\Entities\Pokemon\Pokemon;
use App\Entities\Pokemon\PokemonList;
use App\Model\UserPokemon as UserPokemonModel;
use App\Model\Favorite as FavoriteModel;
use App\Model\Pokemon as PokemonModel;
use App\Model\User as UserModel;
use Illuminate\Support\Facades\DB;

final class UserPokemonRepository implements UserPokemonRepositoryInterface
{
    /** @var UserPokemonList $userPoekmonList */
    private $userPoekmonList;

    /** @var PokemonList $pokemonList */
    private $pokemonList;

    /**
     * @param UserPokemonList $userPoekmonList
     * @param PokemonList $pokemonList
     */
    public function __construct(
        UserPokemonList $userPoekmonList,
        PokemonList $pokemonList
    )
    {
        $this->userPoekmonList = $userPoekmonList;
        $this->pokemonList = $pokemonList;
    }

    /**
     * ポケモン検索結果取得
     *
     * @param integer $searchId
     * @param integer $myId
     * @return UserPokemonList
     */
    public function getUserPokemons(int $searchId, int $myId)
    {
        $my = SetEntity::setUser(UserModel::find($myId));
        if (self::isSearched($searchId)) {
            $userPokemons = UserPokemonModel::whereNotIn('user_id', [$my->getId()])->orderBy('id', 'desc')->paginate(10);
        } else {
            $userPokemons = UserPokemonModel::where('pokemon_id', $searchId)->whereNotIn('user_id', [$my->getId()])->orderBy('id', 'desc')->paginate(10);
        }

        $this->userPoekmonList->setTotal($userPokemons->total());
        $this->userPoekmonList->setLink($userPokemons->links());
        
        foreach ($userPokemons as $userPokemon) {
            if (self::isFavorited($userPokemon, $myId)) {
                $favoriteStatus = FavoriteStatus::RELEASE_STATUS;
                $modalButtonClass = HtmlAttributeConst::RELEASE_USER_POKEMON_MODAL_BUTTON_CLASS;
                $modalName = ModalNameConst::RELEASE_MODAL_NAME;
            } else {
                $favoriteStatus = FavoriteStatus::FAVORITE_STATUS;
                $modalButtonClass = HtmlAttributeConst::FAVORITE_USER_POKEMON_MODAL_BUTTON_CLASS;
                $modalName = ModalNameConst::FAVORITE_MODAL_NAME;
            }

            $this->userPoekmonList->add(SetEntity::setUserPokemon(
                $userPokemon,
                $favoriteStatus,
                $modalButtonClass,
                $modalName
            ));
        }

        return $this->userPoekmonList;
    }

    /**
     * ポケモンマスタリスト取得
     *
     * @return PokemonList
     */
    public function getPokemons()
    {
        $pokemons = PokemonModel::all();

        foreach ($pokemons as $pokemon) {
            $this->pokemonList->add(SetEntity::setPokemon($pokemon));
        }

        return $this->pokemonList;
    }

    /**
     * 検索対象マスタデータ取得
     *
     * @param integer $searchId
     * @return Pokemon
     */
    public function getSearch(int $searchId)
    {
        $pokemon = PokemonModel::find($searchId);

        return SetEntity::setPokemon($pokemon);
    }

    /**
     * ポケモンお気に入り追加処理
     *
     * @param integer $userPokemonId
     * @param integer $myId
     * @return UserPokemon
     */
    public function addFavorite(int $userPokemonId, int $myId)
    {
        return DB::transaction(function() use($userPokemonId, $myId) {
            $my = SetEntity::setUser(UserModel::find($myId));
			$favoriteUserPokemon = new FavoriteModel();
			$favoriteUserPokemon->user_id = $my->getId();
			$favoriteUserPokemon->user_pokemon_id = $userPokemonId;
            $favoriteUserPokemon->save();

            $userPokemon = UserPokemonModel::find($favoriteUserPokemon->user_pokemon_id);

            return SetEntity::setUserPokemon(
                $userPokemon,
                FavoriteStatus::RELEASE_STATUS,
                HtmlAttributeConst::RELEASE_USER_POKEMON_MODAL_BUTTON_CLASS,
                ModalNameConst::RELEASE_MODAL_NAME
            );
		});
    }

    /**
     * ポケモンお気に入り解除処理
     *
     * @param integer $userPokemonId
     * @param integer $myId
     * @return UserPokemon
     */
    public function releaseFavorite(int $userPokemonId, int $myId)
    {
        return DB::transaction(function() use($userPokemonId, $myId) {
            $my = SetEntity::setUser(UserModel::find($myId));
			$releaseUserPokemon = FavoriteModel::where('user_pokemon_id', $userPokemonId)->where('user_id', $my->getId())->first();
            $releaseUserPokemon->delete();
            
            $userPokemon = UserPokemonModel::find($releaseUserPokemon->user_pokemon_id);

            return SetEntity::setUserPokemon(
                $userPokemon,
                FavoriteStatus::FAVORITE_STATUS,
                HtmlAttributeConst::FAVORITE_USER_POKEMON_MODAL_BUTTON_CLASS,
                ModalNameConst::FAVORITE_MODAL_NAME
            );
		});
    }

    /**
     * 検索判定処理
     *
     * @param integer $searchIdId
     * @return boolean
     */
    private static function isSearched(int $searchId)
    {
        return empty($searchId);
    }

    /**
     * ポケモンお気に入り判定処理
     *
     * @param UserPokemonModel $userPokemon
     * @param integer $myId
     * @return boolean
     */
    private static function isFavorited(UserPokemonModel $userPokemon, int $myId)
    {
        return ($userPokemon->favorites->where('user_id', $myId)->count() !== 0);
    }
}