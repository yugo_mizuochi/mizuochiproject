<?php
namespace App\Repositories\FavoriteMyParty;

use App\Entities\FavoriteMyParty\FavoriteMyParty;
use App\Entities\FavoriteMyParty\FavoriteMyPartyList;
use App\Entities\MyParty\MyParty;
use App\Entities\ReleaseMyParty\ReleaseMyParty;

interface FavoriteMyPartyReositoryInterface
{
    /**
     * お気に入りパーティリスト取得処理
     *
     * @param integer $myId
     * @return FavoriteMyPartyList
     */
    public function getFavoriteMyParties(int $myId);

    /**
     * お気に入りパーティをマイパーティに登録処理
     *
     * @param integer $favoriteId
     * @param integer $myId
     * @return MyParty
     */
    public function addFavoriteToMyParty(int $favoriteId, int $myId);

    /**
     * お気に入りパーティ解除処理
     *
     * @param integer $favoriteId
     * @return ReleaseMyParty
     */
    public function releaseFavoriteMyParty(int $favoriteId);
}