<?php
namespace App\Repositories\FavoriteMyParty;

use App\Entities\SetEntity;
use App\Entities\FavoriteMyParty\FavoriteMyParty;
use App\Entities\FavoriteMyParty\FavoriteMyPartyList;
use App\Entities\MyParty\MyParty;
use App\Entities\ReleaseMyParty\ReleaseMyParty;
use App\Model\FavoriteParty as FavoritePartyModel;
use App\Model\Party as PartyModel;
use App\Model\User as UserModel;
use Illuminate\Support\Facades\DB;

final class FavoriteMyPartyRepository implements FavoriteMyPartyReositoryInterface
{
    /** @var FavoriteMyPartyList $favoriteMyPartyList */
    private $favoriteMyPartyList;

    /**
     * @param FavoriteMyPartyList $favoriteMyPartyList
     */
    public function __construct(FavoriteMyPartyList $favoriteMyPartyList)
    {
        $this->favoriteMyPartyList = $favoriteMyPartyList;
    }

    /**
     * お気に入りパーティリスト取得処理
     *
     * @param integer $myId
     * @return FavoriteMyPartyList
     */
    public function getFavoriteMyParties(int $myId)
    {
        $my = SetEntity::setUser(UserModel::find($myId));
        $favoriteMyParties = FavoritePartyModel::where('user_id', [$my->getId()])->orderBy('id', 'desc')->paginate(10);

        $this->favoriteMyPartyList->setLink($favoriteMyParties->links());

        foreach ($favoriteMyParties as $favoriteMyParty) {
            $this->favoriteMyPartyList->add(SetEntity::setFavoriteMyParty($favoriteMyParty));
        }

        return $this->favoriteMyPartyList;
    }

    /**
     * お気に入りパーティをマイパーティに登録処理
     *
     * @param integer $favoriteId
     * @param integer $myId
     * @return MyParty
     */
    public function addFavoriteToMyParty(int $favoriteId, int $myId)
    {
        return DB::transaction(function () use ($favoriteId, $myId) {
            $favorite = FavoritePartyModel::find($favoriteId);
            $my = SetEntity::setUser(UserModel::find($myId));
            $favoriteMyParty = SetEntity::setFavoriteMyParty($favorite);

            $myParty = new PartyModel();
            $myParty->user_id = $my->getId();
            $myParty->name = $favoriteMyParty->getUserParty()->getUserPartyName();
            $myParty->first_user_pokemon_id = $favoriteMyParty->getUserParty()->getFirstUserPokemon()->getId();
            $myParty->second_user_pokemon_id = $favoriteMyParty->getUserParty()->getSecondUserPokemon()->getId();
            $myParty->third_user_pokemon_id = $favoriteMyParty->getUserParty()->getThirdUserPokemon()->getId();
            $myParty->fourth_user_pokemon_id = $favoriteMyParty->getUserParty()->getFourthUserPokemon()->getId();
            $myParty->fifth_user_pokemon_id = $favoriteMyParty->getUserParty()->getFifthUserPokemon()->getId();
            $myParty->sixth_user_pokemon_id = $favoriteMyParty->getUserParty()->getSixthUserPokemon()->getId();
            $myParty->memo = $favoriteMyParty->getUserParty()->getMemo();
            $myParty->save();

            $favorite->delete();

            return SetEntity::setMyParty($myParty);
        });
    }

    /**
     * お気に入りパーティ解除処理
     *
     * @param integer $favoriteId
     * @return ReleaseMyParty
     */
    public function releaseFavoriteMyParty(int $favoriteId)
    {
        return DB::transaction(function () use($favoriteId) {
            $favorite = FavoritePartyModel::find($favoriteId);
            $favorite->delete();

            $releaseMyParty = PartyModel::withTrashed()->find($favorite->user_party_id);
            return SetEntity::setReleaseMyParty($releaseMyParty);
        });
    }
}