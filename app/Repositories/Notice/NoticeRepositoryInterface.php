<?php
namespace App\Repositories\Notice;

interface NoticeRepositoryInterface
{
    /**
     * お知らせ一覧取得処理
     *
     * @return NoticeList
     */
    public function getNoticies();
}