<?php
namespace App\Repositories\Notice;

use App\Entities\SetEntity;
use App\Model\Notice as NoticeModel;
use App\Entities\Notice\NoticeList;

final class NoticeReository implements NoticeRepositoryInterface
{
    /** @var NoticeList $noticeList */
    private $noticeList;

    /**
     * @param NoticeList $noticeList
     */
    public function __construct(NoticeList $noticeList)
    {
        $this->noticeList = $noticeList;
    }

    /**
     * お知らせ一覧取得処理
     *
     * @return NoticeList
     */
    public function getNoticies()
    {
        $notices = NoticeModel::orderBy('id', 'desc')->get();

        foreach ($notices as $notice) {
            $this->noticeList->add(SetEntity::setNotice($notice));
        }

        return $this->noticeList;
    }
}