<?php
namespace App\Repositories\AdminUser;

use App\Entities\SetEntity;
use App\Entities\User\User;
use App\Entities\User\UserList;
use App\Model\User as UserModel;
use App\Enums\UserType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

final class AdminUserRepository implements AdminUserRepositoryInterface
{
    /** @var UserList $userList */
    private $userList;

    /** @var array $userTypes */
    private $userTypes;

    /**
     * @param UserList $userList
     */
    public function __construct(UserList $userList)
    {
        $this->userList = $userList;
        $this->userTypes = UserType::getUserTypes();
    }

    /**
     * ユーザーリスト取得処理
     *
     * @return UserList
     */
    public function getUsers()
    {
        $users = UserModel::orderBy('id', 'desc')->paginate(10);
        $this->userList->setLink($users->links());

        foreach ($users as $user) {
            $this->userList->add(SetEntity::setUser($user));
        }

        return $this->userList;
    }

    /**
     * ユーザータイプリスト取得処理
     *
     * @return array
     */
    public function getUserTypes()
    {
        return $this->userTypes;
    }

    /**
     * ユーザー新規登録処理
     *
     * @param array $addParams
     * @return User
     */
    public function addUser(array $addParams)
    {
        return DB::transaction(function () use($addParams) {
            $user = new UserModel();
            $user->name = $addParams['user_name'];
            $user->email = $addParams['user_email'];
            $user->password = Hash::make($addParams['user_password']);
            $user->user_type = $addParams['user_type'];
            $user->save();

            return SetEntity::setUser($user);
        });
    }

    /**
     * 編集対象のユーザー情報取得処理
     *
     * @param integer $userId
     * @return User
     */
    public function getUser(int $userId)
    {
        return SetEntity::setUser(UserModel::find($userId));
    }

    /**
     * ユーザー編集処理
     *
     * @param array $editParams
     * @return User
     */
    public function editUser(array $editParams)
    {
        return DB::transaction(function () use($editParams) {
            $user = UserModel::find($editParams['user_id']);
            $user->name = $editParams['user_name'];
            $user->email = $editParams['user_email'];
            $user->user_type = $editParams['user_type'];
            $user->save();

            return SetEntity::setUser($user);
        });
    }

    /**
     * ユーザー削除処理
     *
     * @param integer $userId
     * @return User
     */
    public function deleteUser(int $userId)
    {
        return DB::transaction(function () use($userId) {
            $user = UserModel::find($userId);
            $this::deleteUserFavoritePokemons($user);
            $this::deleteUserFavoriteParties($user);
            $user->delete();

            return SetEntity::setUser($user);
        });
    }

    /**
     * 削除対象ユーザーのお気に入りポケモン削除処理
     *
     * @param UserModel $user
     */
    private static function deleteUserFavoritePokemons(UserModel $user)
	{
		$favoritePokemons = $user->favorites()->get();
		foreach($favoritePokemons as $favoritePokemon) {
			$favoritePokemon->delete();
		}
    }
    
    /**
     * 削除対象ユーザーのお気に入りパーティ削除処理
     *
     * @param UserModel $user
     */
    private static function deleteUserFavoriteParties(UserModel $user)
	{
		$favoriteParties = $user->favoriteParties()->get();
		foreach($favoriteParties as $favoriteParty) {
			$favoriteParty->delete();
		}
	}
}