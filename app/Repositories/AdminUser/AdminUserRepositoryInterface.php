<?php
namespace App\Repositories\AdminUser;

use App\Entities\User\User;
use App\Entities\User\UserList;

interface AdminUserRepositoryInterface
{
    /**
     * ユーザーリスト取得処理
     *
     * @return UserList
     */
    public function getUsers();

    /**
     * ユーザータイプリスト取得処理
     *
     * @return array
     */
    public function getUserTypes();

    /**
     * ユーザー新規登録処理
     *
     * @param array $addParams
     * @return User
     */
    public function addUser(array $addParams);

    /**
     * 編集対象のユーザー情報取得処理
     *
     * @param integer $userId
     * @return User
     */
    public function getUser(int $userId);

    /**
     * ユーザー編集処理
     *
     * @param array $editParams
     * @return User
     */
    public function editUser(array $editParams);

    /**
     * ユーザー削除処理
     *
     * @param integer $userId
     * @return User
     */
    public function deleteUser(int $userId);
}