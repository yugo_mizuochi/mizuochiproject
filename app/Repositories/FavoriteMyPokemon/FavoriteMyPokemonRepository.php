<?php
namespace App\Repositories\FavoriteMyPokemon;

use App\Entities\SetEntity;
use App\Entities\FavoriteMyPokemon\FavoriteMyPokemonList;
use App\Entities\MyPokemon\MyPokemon;
use App\Entities\ReleaseMyPokemon\ReleaseMyPokemon;
use App\Model\UserPokemon as UserPokemonModel;
use App\Model\Favorite as FavoriteModel;
use App\Model\User as UserModel;
use Illuminate\Support\Facades\DB;

final class FavoriteMyPokemonRepository implements FavoriteMyPokemonRepositoryInterface
{
    /** @var FavoriteMyPokemonList $favoriteMyPokemonList */
    private $favoriteMyPokemonList;

    /**
     * @param FavoriteMyPokemonList $favoriteMyPokemonList
     */
    public function __construct(FavoriteMyPokemonList $favoriteMyPokemonList)
    {
        $this->favoriteMyPokemonList = $favoriteMyPokemonList;
    }

    /**
     * お気に入りポケモンリスト取得処理
     *
     * @param integer $myId
     * @return FavoriteMyPokemonList
     */
    public function getFavoriteMyPokemons(int $myId)
    {
        $my = SetEntity::setUser(UserModel::find($myId));
        $favorites = FavoriteModel::where('user_id', $my->getId())->orderBy('id', 'desc')->paginate(10);

        $this->favoriteMyPokemonList->setlink($favorites->links());

        foreach ($favorites as $favorite) {
            $this->favoriteMyPokemonList->add(SetEntity::setFavoriteMyPokemon($favorite));
        }

        return $this->favoriteMyPokemonList;
    }

    /**
     * お気に入りポケモンをマイポケモン登録処理
     *
     * @param integer $favoriteId
     * @param integer $myId
     * @return MyPokemon
     */
    public function addFavoriteToMyPokemon(int $favoriteId, int $myId)
    {
        return DB::transaction(function () use ($favoriteId, $myId) {
            $favorite = FavoriteModel::find($favoriteId);
            $my = SetEntity::setUser(UserModel::find($myId));
            $favoriteMyPokemon = SetEntity::setFavoriteMyPokemon($favorite);
    
            $myPokemon = new UserPokemonModel();
            $myPokemon->user_id = $my->getId();
            $myPokemon->pokemon_id = $favoriteMyPokemon->getUserPokemon()->getPokemon()->getId();
            $myPokemon->pokemon_name = $favoriteMyPokemon->getUserPokemon()->getUserPokemonName();
            $myPokemon->item_id = $favoriteMyPokemon->getUserPokemon()->getItem()->getId();
            $myPokemon->hp = $favoriteMyPokemon->getUserPokemon()->getHp();
            $myPokemon->attack = $favoriteMyPokemon->getUserPokemon()->getAttack();
            $myPokemon->defense = $favoriteMyPokemon->getUserPokemon()->getDefense();
            $myPokemon->special_attack = $favoriteMyPokemon->getUserPokemon()->getSpecialAttack();
            $myPokemon->special_defense = $favoriteMyPokemon->getUserPokemon()->getSpecialDefense();
            $myPokemon->agility = $favoriteMyPokemon->getUserPokemon()->getAgility();
            $myPokemon->memo = $favoriteMyPokemon->getUserPokemon()->getMemo();
            $myPokemon->save();

            $favorite->delete();

            return SetEntity::setMyPokemon($myPokemon);
        });
    }

    /**
     * お気に入りポケモン解除処理
     *
     * @param integer $favoriteId
     * @return ReleaseMyPokemon
     */
    public function releaseFavoriteMyPokemon(int $favoriteId)
    {
        return DB::transaction(function () use ($favoriteId) {
            $favorite = FavoriteModel::find($favoriteId);
            $favorite->delete();

            $releaseMyPokemon = UserPokemonModel::withTrashed()->find($favorite->user_pokemon_id);

            return SetEntity::setReleaseMyPokemon($releaseMyPokemon);
        });
    }
}