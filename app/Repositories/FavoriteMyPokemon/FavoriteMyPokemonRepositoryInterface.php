<?php
namespace App\Repositories\FavoriteMyPokemon;

use App\Entities\FavoriteMyPokemon\FavoriteMyPokemon;
use App\Entities\FavoriteMyPokemon\FavoriteMyPokemonList;
use App\Entities\MyPokemon\MyPokemon;

interface FavoriteMyPokemonRepositoryInterface
{
    /**
     * お気に入りポケモンリスト取得
     *
     * @param integer $myId
     * @return FavoriteMyPokemonList
     */
    public function getFavoriteMyPokemons(int $myId);

    /**
     * お気に入りポケモンをマイポケモン登録処理
     *
     * @param integer $favoriteId
     * @param integer $myId
     * @return MyPokemon
     */
    public function addFavoriteToMyPokemon(int $favoriteId, int $myId);

    /**
     * お気に入りポケモン解除処理
     *
     * @param integer $favoriteId
     * @return ReleaseMyPokemon
     */
    public function releaseFavoriteMyPokemon(int $favoriteId);
}