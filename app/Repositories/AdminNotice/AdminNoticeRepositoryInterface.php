<?php
namespace App\Repositories\AdminNotice;

use App\Entities\Notice\Notice;
use App\Entities\Notice\NoticeList;

interface AdminNoticeRepositoryInterface
{
    /**
     * お知らせ一覧リスト取得処理
     *
     * @return NoticeList
     */
    public function getNotices();

    /**
     * お知らせ新規登録処理
     *
     * @param array $addParams
     * @return Notice
     */
    public function addNotice(array $addParams);

    /**
     * 編集お知らせ取得処理
     *
     * @param integer $noticeId
     * @return Notice
     */
    public function getNotice(int $noticeId);

    /**
     * お知らせ編集処理
     *
     * @param array $editParams
     * @return Notice
     */
    public function editNotice(array $editParams);

    /**
     * お知らせ削除処理
     *
     * @param integer $noticeId
     * @return Notice
     */
    public function deleteNotice(int $noticeId);
}