<?php
namespace App\Repositories\AdminNotice;

use App\Entities\SetEntity;
use App\Entities\Notice\Notice;
use App\Entities\Notice\NoticeList;
use App\Model\Notice as NoticeModel;
use Illuminate\Support\Facades\DB;

final class AdminNoticeRepository implements AdminNoticeRepositoryInterface
{
    /** @var NoticeList $noticeList */
    private $noticeList;

    /**
     * @param NoticeList $noticeList
     */
    public function __construct(NoticeList $noticeList)
    {
        $this->noticeList = $noticeList;
    }

    /**
     * お知らせ一覧リスト取得処理
     *
     * @return NoticeList
     */
    public function getNotices()
    {
        $notices = NoticeModel::orderBy('id', 'desc')->paginate(10);
        $this->noticeList->setLink($notices->links());

        foreach ($notices as $notice) {
            $this->noticeList->add(
                SetEntity::setNotice($notice)
            );
        }

        return $this->noticeList;
    }

    /**
     * お知らせ新規登録処理
     *
     * @param array $addParams
     * @return Notice
     */
    public function addNotice(array $addParams)
    {
        return DB::transaction(function () use($addParams) {
            $notice = new NoticeModel();
            $notice->body = $addParams['notice_body'];
            $notice->save();

            return SetEntity::setNotice($notice);
        });
    }

    /**
     * 編集お知らせ取得処理
     *
     * @param integer $noticeId
     * @return Notice
     */
    public function getNotice(int $noticeId)
    {
        $notice = NoticeModel::find($noticeId);

        return SetEntity::setNotice($notice);
    }

    /**
     * お知らせ編集処理
     *
     * @param array $editParams
     * @return Notice
     */
    public function editNotice(array $editParams)
    {
        return DB::transaction(function () use($editParams) {
            $notice = NoticeModel::find($editParams['notice_id']);
            $notice->body = $editParams['notice_body'];
            $notice->save();

            return SetEntity::setNotice($notice);
        });
    }

    /**
     * お知らせ削除処理
     *
     * @param integer $noticeId
     * @return Notice
     */
    public function deleteNotice(int $noticeId)
    {
        return DB::transaction(function () use($noticeId) {
            $notice = NoticeModel::find($noticeId);
            $notice->delete();
            return SetEntity::setNotice($notice);
        });
    }
}