<?php
namespace App\Repositories\MyPokemon;

use App\Entities\SetEntity;
use App\Entities\Item\ItemList;
use App\Entities\Pokemon\PokemonList;
use App\Entities\MyPokemon\MyPokemon;
use App\Entities\MyPokemon\MyPokemonList;
use App\Model\User as UserModel;
use App\Model\UserPokemon as UserPokemonModel;
use App\Model\Pokemon as PokemonModel;
use App\Model\Item as ItemModel;
use Illuminate\Support\Facades\DB;

final class MyPokemonRepository implements MyPokemonRepositoryInterface
{
    /** @var MyPokemonList $myPokemonList */
    private $myPokemonList;

    /** @var PokemonList $pokemonList */
    private $pokemonList;

    /** @var ItemList $itemList */
    private $itemList;

    /**
     * @param MyPokemonList $myPokemonList
     * @param PokemonList $pokemonList
     * @param ItemList $itemList
     */
    public function __construct(
        MyPokemonList $myPokemonList,
        PokemonList $pokemonList,
        ItemList $itemList
    )
    {
        $this->myPokemonList = $myPokemonList;
        $this->pokemonList = $pokemonList;
        $this->itemList = $itemList;
    }

    /**
     * マイポケモンリスト取得処理
     *
     * @param integer $myId
     * @return MyPokemonList
     */
    public function getMyPokemons(int $myId)
    {
        $my = SetEntity::setUser(UserModel::find($myId));
        $myPokemons = UserPokemonModel::where('user_id', $my->getId())->orderBy('id', 'desc')->paginate(10);

        $this->myPokemonList->setTotal((int) $myPokemons->total());
        $this->myPokemonList->setLink($myPokemons->links());

        foreach ($myPokemons as $myPokemon) {
            $this->myPokemonList->add(SetEntity::setMyPokemon($myPokemon));
        }

        return $this->myPokemonList;
    }

    /**
     * マイポケモン情報取得処理
     *
     * @param integer $myPokemonId
     * @return MyPokemon
     */
    public function getMyPokemon(int $myPokemonId)
    {
        $myPokemon = UserPokemonModel::find($myPokemonId);

        return SetEntity::setMyPokemon($myPokemon);
    }

    /**
     * ポケモンマスタリスト取得処理
     *
     * @return PokemonList
     */
    public function getPokemons()
    {
        $pokemons = PokemonModel::all();

        foreach ($pokemons as $pokemon) {
            $this->pokemonList->add(SetEntity::setPokemon($pokemon));
        }

        return $this->pokemonList;
    }

    /**
     * アイテムマスタリスト取得処理
     *
     * @return ItemList
     */
    public function getItems()
    {
        $items = ItemModel::all();

        foreach ($items as $item) {
            $this->itemList->add(SetEntity::setItem($item));
        }

        return $this->itemList;
    }

    /**
     * マイポケモン新規登録処理
     *
     * @param array $addParams
     * @return MyPokemon
     */
    public function addMyPokemon(array $addParams)
    {
        return DB::transaction(function() use($addParams) {
            $item = SetEntity::setItem(ItemModel::find($addParams['item_id']));
            $pokemon = SetEntity::setPokemon(PokemonModel::find($addParams['pokemon_id']));
            $my = SetEntity::setUser(UserModel::find($addParams['my_id']));
            
			$myPokemon = new UserPokemonModel();
			$myPokemon->user_id = $my->getId();
			$myPokemon->pokemon_id = $pokemon->getId();
			$myPokemon->pokemon_name = $addParams['my_pokemon_name'];
			$myPokemon->item_id = $item->getId();
			$myPokemon->hp = $addParams['hp_value'];
			$myPokemon->attack = $addParams['attack_value'];
			$myPokemon->defense = $addParams['defense_value'];
			$myPokemon->special_attack = $addParams['special_attack_value'];
			$myPokemon->special_defense = $addParams['special_defense_value'];
			$myPokemon->agility = $addParams['agility_value'];
			$myPokemon->memo = $addParams['my_pokemon_memo'];
			$myPokemon->save();

			return SetEntity::setMyPokemon($myPokemon);
		});
    }

    /**
     * マイポケモン編集処理
     *
     * @param array $editParams
     * @return MyPokemon
     */
    public function editMyPokemon(array $editParams)
    {
        return DB::transaction(function() use($editParams) {
            $item = SetEntity::setItem(ItemModel::find($editParams['item_id']));
            $pokemon = SetEntity::setPokemon(PokemonModel::find($editParams['pokemon_id']));
            $my = SetEntity::setUser(UserModel::find($editParams['my_id']));
            $myPokemon = UserPokemonModel::find($editParams['my_pokemon_id']);

            $myPokemon->user_id = $my->getId();
			$myPokemon->pokemon_id = $pokemon->getId();
			$myPokemon->pokemon_name = $editParams['my_pokemon_name'];
			$myPokemon->item_id = $item->getId();
			$myPokemon->hp = $editParams['hp_value'];
			$myPokemon->attack = $editParams['attack_value'];
			$myPokemon->defense = $editParams['defense_value'];
			$myPokemon->special_attack = $editParams['special_attack_value'];
			$myPokemon->special_defense = $editParams['special_defense_value'];
			$myPokemon->agility = $editParams['agility_value'];
			$myPokemon->memo = $editParams['my_pokemon_memo'];
            $myPokemon->save();
            
            return SetEntity::setMyPokemon($myPokemon);
		});
    }

    /**
     * マイポケモン削除処理
     *
     * @param integer $myPokemonId
     * @return MyPokemon
     */
    public function deleteMyPokemon(int $myPokemonId)
    {
        return DB::transaction(function() use($myPokemonId) {
			$myPokemon = UserPokemonModel::find($myPokemonId);
			$myPokemon->delete();
			return SetEntity::setMyPokemon($myPokemon);
		});
    }

}