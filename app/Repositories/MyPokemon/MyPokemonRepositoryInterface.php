<?php
namespace App\Repositories\MyPokemon;

use App\Entities\Item\ItemList;
use App\Entities\Pokemon\PokemonList;
use App\Entities\MyPokemon\MyPokemon;
use App\Entities\MyPokemon\MyPokemonList;

interface MyPokemonRepositoryInterface
{
    /**
     * マイポケモンリスト取得処理
     *
     * @param integer $myId
     * @return MyPokemonList
     */
    public function getMyPokemons(int $myId);

    /**
     * ポケモンマスタリスト取得処理
     *
     * @return PokemonList
     */
    public function getPokemons();

    /**
     * マイポケモン情報取得
     *
     * @param integer $myPokemonId
     * @return MyPokemon
     */
    public function getMyPokemon(int $myPokemonId);

    /**
     * アイテムマスタリスト取得処理
     *
     * @return ItemList
     */
    public function getItems();

    /**
     * マイポケモン新規登録処理
     *
     * @param array $params
     * @return MyPokemon
     */
    public function addMyPokemon(array $params);

    /**
     * マイポケモン編集処理
     *
     * @param array $editParams
     * @return MyPokemon
     */
    public function editMyPokemon(array $editParams);

    /**
     * マイポケモン削除処理
     *
     * @param integer $myPokemonId
     * @return MyPokemon
     */
    public function deleteMyPokemon(int $myPokemonId);
}