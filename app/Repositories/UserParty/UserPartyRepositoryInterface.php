<?php
namespace App\Repositories\UserParty;

interface UserPartyRepositoryInterface
{
    /**
     * パーティ検索結果取得
     *
     * @param integer $searchId
     * @param integer $myId
     * @return UserPartyList
     */
    public function getUserParties(int $searchId, int $myId);

    /**
     * ポケモンマスタリスト取得
     *
     * @return PokemonList
     */
    public function getPokemons();

    /**
     * 検索対象マスタデータ取得
     *
     * @param integer $searchId
     * @return Pokemon
     */
    public function getSearch(int $searchId);

    /**
     * パーティお気に入り追加処理
     *
     * @param integer $userPartyId
     * @param integer $myId
     * @return UserParty
     */
    public function addFavoriteUserParty(int $userPartyId, int $myId);

    /**
     * パーティお気に入り解除処理
     *
     * @param integer $userPartyId
     * @param integer $myId
     * @return UserParty
     */
    public function releaseFavoriteUserParty(int $userPartyId, int $myId);
}