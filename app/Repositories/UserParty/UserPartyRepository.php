<?php
namespace App\Repositories\UserParty;

use App\Consts\HtmlAttributeConst;
use App\Consts\ModalNameConst;
use App\Enums\FavoriteStatus;
use App\Entities\SetEntity;
use App\Entities\UserParty\UserParty;
use App\Entities\UserParty\UserPartyList;
use App\Entities\Pokemon\PokemonList;
use App\Model\User as UserModel;
use App\Model\Pokemon as PokemonModel;
use App\Model\Party as PartyModel;
use App\Model\UserPokemon as UserPokemonModel;
use App\Model\FavoriteParty as FavoritePartyModel;
use Illuminate\Support\Facades\DB;

final class UserPartyRepository implements UserPartyRepositoryInterface
{
    /** @var UserPartyList $userPartyList */
    private $userPartyList;

    /** @var PokemonList $pokemonList */
    private $pokemonList;

    /**
     * @param UserPartyList $userPartyList
     * @param PokemonList $pokemonList
     */
    public function __construct(
        UserPartyList $userPartyList,
        PokemonList $pokemonList
    )
    {
        $this->userPartyList = $userPartyList;
        $this->pokemonList = $pokemonList;
    }

    /**
     * パーティ検索結果取得
     *
     * @param integer $searchId
     * @param integer $myId
     * @return UserPartyList
     */
    public function getUserParties(int $searchId, int $myId)
    {
        $my = SetEntity::setUser(UserModel::find($myId));

        $userPokemons = UserPokemonModel::whereNotIn('user_id', [$my->getId()])->where('pokemon_id', $searchId)->get();
        $userPokemonIds = [];
        foreach ($userPokemons as $userPokemon) {
			$userPokemonIds[] = $userPokemon->id;
		}
        if (self::isSearched($searchId)) {
            $userParties = PartyModel::whereNotIn('user_id', [$my->getId()])->orderBy('id', 'desc')->paginate(10);
        } else {
            $userParties = PartyModel::whereIn('first_user_pokemon_id', $userPokemonIds)
                ->orWhereIn('second_user_pokemon_id', $userPokemonIds)
                ->orWhereIn('third_user_pokemon_id', $userPokemonIds)
                ->orWhereIn('fourth_user_pokemon_id', $userPokemonIds)
                ->orWhereIn('fifth_user_pokemon_id', $userPokemonIds)
                ->orWhereIn('sixth_user_pokemon_id', $userPokemonIds)
                ->whereNotIn('user_id', [$my->getId()])
                ->orderBy('id', 'desc')
                ->paginate(10);
        }

        $this->userPartyList->setTotal((int) $userParties->total());
        $this->userPartyList->setLink($userParties->links());

        foreach ($userParties as $userParty) {
            if (self::isFavorited($userParty, $my->getId())) {
                $favoriteStatus = FavoriteStatus::RELEASE_STATUS;
                $modalButtonClass = HtmlAttributeConst::RELEASE_USER_PARTY_MODAL_BUTTON_CLASS;
                $modalName = ModalNameConst::RELEASE_MODAL_NAME;
            } else {
                $favoriteStatus = FavoriteStatus::FAVORITE_STATUS;
                $modalButtonClass = HtmlAttributeConst::FAVORITE_USER_PARTY_MODAL_BUTTON_CLASS;
                $modalName = ModalNameConst::FAVORITE_MODAL_NAME;
            }

            $this->userPartyList->add(SetEntity::setUserParty(
                $userParty,
                $favoriteStatus,
                $modalButtonClass,
                $modalName
            ));
        }

        return $this->userPartyList;
    }

    /**
     * ポケモンマスタリスト取得
     *
     * @return PokemonList
     */
    public function getPokemons()
    {
        $pokemons = PokemonModel::all();

        foreach ($pokemons as $pokemon) {
            $this->pokemonList->add(SetEntity::setPokemon($pokemon));
        }

        return $this->pokemonList;
    }

    /**
     * 検索対象マスタデータ取得
     *
     * @param integer $searchId
     * @return Pokemon
     */
    public function getSearch(int $searchId)
    {
        $pokemon = PokemonModel::find($searchId);

        return SetEntity::setPokemon($pokemon);
    }

    /**
     * パーティお気に入り追加処理
     *
     * @param integer $userPartyId
     * @param integer $myId
     * @return UserParty
     */
    public function addFavoriteUserParty(int $userPartyId, int $myId)
    {
        return DB::transaction(function () use($userPartyId, $myId) {
            $my = SetEntity::setUser(UserModel::find($myId));
            $favoriteUserParty = new FavoritePartyModel();
            $favoriteUserParty->user_id = $my->getId();
            $favoriteUserParty->user_party_id = $userPartyId;
            $favoriteUserParty->save();

            $userParty = PartyModel::find($favoriteUserParty->user_party_id);

            return SetEntity::setUserParty(
                $userParty,
                FavoriteStatus::RELEASE_STATUS,
                HtmlAttributeConst::RELEASE_USER_PARTY_MODAL_BUTTON_CLASS,
                ModalNameConst::RELEASE_MODAL_NAME
            );
        });
    }

    /**
     * パーティお気に入り解除処理
     *
     * @param integer $userPartyId
     * @param integer $myId
     * @return UserParty
     */
    public function releaseFavoriteUserParty(int $userPartyId, int $myId)
    {
        return DB::transaction(function () use($userPartyId, $myId) {
            $my = SetEntity::setUser(UserModel::find($myId));
            $releaseUserParty = FavoritePartyModel::where('user_party_id', $userPartyId)->where('user_id', $my->getId())->first();
            $releaseUserParty->delete();

            $userParty = PartyModel::find($releaseUserParty->user_party_id);

            return SetEntity::setUserParty(
                $userParty,
                FavoriteStatus::FAVORITE_STATUS,
                HtmlAttributeConst::FAVORITE_USER_PARTY_MODAL_BUTTON_CLASS,
                ModalNameConst::FAVORITE_MODAL_NAME
            );
        });
    }

    /**
     * 検索判定処理
     *
     * @param integer $searchId
     * @return boolean
     */
    private static function isSearched(int $searchId)
    {
        return empty($searchId);
    }

    /**
     * パーティお気に入り判定処理
     *
     * @param PartyModel $userParty
     * @param integer $myId
     * @return boolean
     */
    private static function isFavorited(PartyModel $userParty, int $myId)
    {
        return ($userParty->favoriteParties->where('user_id', $myId)->count() !== 0);
    }
}