<?php
namespace App\Repositories\MyParty;

interface MyPartyRepositoryInterface
{
    /**
     * マイパーティリスト取得処理
     *
     * @param integer $myId
     * @return MyPartyList
     */
    public function getMyParties(int $myId);

    /**
     * マイポケモンリスト取得処理
     *
     * @param integer $myId
     * @return MyPokemonList
     */
    public function getMyPokemons(int $myId);

    /**
     * マイパーティ新規登録処理
     *
     * @param array $addParams
     * @return MyParty
     */
    public function addMyParty(array $addParams);

    /**
     * 編集対象マイパーティ取得処理
     *
     * @param integer $myPartyId
     * @return MyParty
     */
    public function getMyParty(int $myPartyId);

    /**
     * マイパーティ編集処理
     *
     * @param array $editParams
     * @return MyParty
     */
    public function editMyParty(array $editParams);

    /**
     * マイパーティ削除処理
     *
     * @param integer $myPartyId
     * @return MyParty
     */
    public function deleteMyParty(int $myPartyId);
}