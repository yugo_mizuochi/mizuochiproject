<?php
namespace App\Repositories\MyParty;

use App\Entities\SetEntity;
use App\Entities\MyParty\MyParty;
use App\Entities\MyParty\MyPartyList;
use App\Entities\MyPokemon\MyPokemonList;
use App\Model\User as UserModel;
use App\Model\Party as PartyModel;
use App\Model\UserPokemon as UserPokemonModel;
use Hamcrest\Core\Set;
use Illuminate\Support\Facades\DB;


final class MyPartyRepository implements MyPartyRepositoryInterface
{
    /** @var MyPartyList $myPartyList */
    private $myPartyList;

    /** @var MyPokemonList $myPokemonList */
    private $myPokemonList;

    /**
     * @param MyPartyList $myPartyList
     * @param MyPokemonList $myPokemonList
     */
    public function __construct(
        MyPartyList $myPartyList,
        MyPokemonList $myPokemonList
    )
    {
        $this->myPartyList = $myPartyList;
        $this->myPokemonList = $myPokemonList;
    }

    /**
     * マイパーティリスト取得処理
     *
     * @param integer $myId
     * @return MyPartyList
     */
    public function getMyParties(int $myId)
    {
        $myParties = PartyModel::where('user_id', $myId)->orderBy('id', 'desc')->paginate(10);

        $this->myPartyList->setTotal((int) $myParties->total());
        $this->myPartyList->setLink($myParties->links());

        foreach ($myParties as $myParty) {
            $this->myPartyList->add(SetEntity::setMyParty($myParty));
        }

        return $this->myPartyList;
    }

    /**
     * マイポケモンリスト取得処理
     *
     * @param integer $myId
     * @return MyPokemonList
     */
    public function getMyPokemons(int $myId)
    {
        $myPokemons = UserPokemonModel::where('user_id', $myId)->get();

        foreach ($myPokemons as $myPokemon) {
            $this->myPokemonList->add(SetEntity::setMyPokemon($myPokemon));
        }

        return $this->myPokemonList;
    }

    /**
     * マイパーティ新規登録処理
     *
     * @param array $addParams
     * @return MyParty
     */
    public function addMyParty(array $addParams)
    {
        return DB::transaction(function () use ($addParams) {
            $my = SetEntity::setUser(UserModel::find($addParams['my_id']));
            $firstMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['first_my_pokemon_id']));
            $secondMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['second_my_pokemon_id']));
            $thirdMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['third_my_pokemon_id']));
            $fourthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['fourth_my_pokemon_id']));
            $fifthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['fifth_my_pokemon_id']));
            $sixthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($addParams['sixth_my_pokemon_id']));

            $myParty = new PartyModel();
            $myParty->user_id = $my->getId();
            $myParty->name = $addParams['my_party_name'];
            $myParty->first_user_pokemon_id = $firstMyPokemon->getId();
            $myParty->second_user_pokemon_id = $secondMyPokemon->getId();
            $myParty->third_user_pokemon_id = $thirdMyPokemon->getId();
            $myParty->fourth_user_pokemon_id = $fourthMyPokemon->getId();
            $myParty->fifth_user_pokemon_id = $fifthMyPokemon->getId();
            $myParty->sixth_user_pokemon_id = $sixthMyPokemon->getId();
            $myParty->memo = $addParams['my_party_memo'];
            $myParty->save();

            return SetEntity::setMyParty($myParty);
        });
    }

    /**
     * 編集対象マイパーティ取得処理
     *
     * @param integer $myPartyId
     * @return MyParty
     */
    public function getMyParty(int $myPartyId)
    {
        $myParty = PartyModel::find($myPartyId);

        return SetEntity::setMyParty($myParty);
    }

    /**
     * マイパーティ編集処理
     *
     * @param array $editParams
     * @return MyParty
     */
    public function editMyParty(array $editParams)
    {
        return DB::transaction(function () use($editParams) {
            $my = SetEntity::setUser(UserModel::find($editParams['my_id']));
            $firstMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['first_my_pokemon_id']));
            $secondMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['second_my_pokemon_id']));
            $thirdMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['third_my_pokemon_id']));
            $fourthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['fourth_my_pokemon_id']));
            $fifthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['fifth_my_pokemon_id']));
            $sixthMyPokemon = SetEntity::setMyPokemon(UserPokemonModel::find($editParams['sixth_my_pokemon_id']));

            $myParty = PartyModel::find($editParams['my_party_id']);
            $myParty->user_id = $my->getId();
            $myParty->name = $editParams['my_party_name'];
            $myParty->first_user_pokemon_id = $firstMyPokemon->getId();
            $myParty->second_user_pokemon_id = $secondMyPokemon->getId();
            $myParty->third_user_pokemon_id = $thirdMyPokemon->getId();
            $myParty->fourth_user_pokemon_id = $fourthMyPokemon->getId();
            $myParty->fifth_user_pokemon_id = $fifthMyPokemon->getId();
            $myParty->sixth_user_pokemon_id = $sixthMyPokemon->getId();
            $myParty->memo = $editParams['my_party_memo'];
            $myParty->save();

            return SetEntity::setMyParty($myParty);
        });
    }

    /**
     * マイパーティ削除処理
     *
     * @param integer $myPartyId
     * @return MyParty
     */
    public function deleteMyParty(int $myPartyId)
    {
        return DB::transaction(function () use($myPartyId) {
            $myParty = PartyModel::find($myPartyId);
            $myParty->delete();
            return SetEntity::setMyParty($myParty);
        });
    }
}