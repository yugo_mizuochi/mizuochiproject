<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Model\UserPokemon;

class PokemonComparison implements Rule
{
    const PARTY_POKEMON_NUMBER = 6;
    /**
     * [$attributes description]
     * @var [type]
     */
    protected $pokemonsIds;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attributes)
    {
        $this->pokemonsIds = [
            UserPokemon::find($attributes['my_pokemon_id']['first_my_pokemon_id'])->pokemon_id,
            UserPokemon::find($attributes['my_pokemon_id']['second_my_pokemon_id'])->pokemon_id,
            UserPokemon::find($attributes['my_pokemon_id']['third_my_pokemon_id'])->pokemon_id,
            UserPokemon::find($attributes['my_pokemon_id']['fourth_my_pokemon_id'])->pokemon_id,
            UserPokemon::find($attributes['my_pokemon_id']['fifth_my_pokemon_id'])->pokemon_id,
            UserPokemon::find($attributes['my_pokemon_id']['sixth_my_pokemon_id'])->pokemon_id
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return count(array_unique($this->pokemonsIds)) === self::PARTY_POKEMON_NUMBER;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '同じポケモンが２匹以上います。';
    }
}
