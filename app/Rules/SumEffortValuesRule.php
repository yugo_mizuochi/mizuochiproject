<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SumEffortValuesRule implements Rule
{
    const MAX_EFFORT_VALUES = 510;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sumEffortValues = 0;
        foreach ($value as $effortValue) {
            $sumEffortValues += $effortValue;
        }

        return $sumEffortValues <= self::MAX_EFFORT_VALUES;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '努力値の合計が510を超えています。';
    }
}
