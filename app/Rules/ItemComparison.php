<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Model\UserPokemon;

class ItemComparison implements Rule
{
    const PARTY_ITEM_NUMBER = 6;
    /**
     * [$itemsIds description]
     * @var [type]
     */
    protected $itemsIds;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attributes)
    {
        $this->itemsIds = [
            UserPokemon::find($attributes['my_pokemon_id']['first_my_pokemon_id'])->item_id,
            UserPokemon::find($attributes['my_pokemon_id']['second_my_pokemon_id'])->item_id,
            UserPokemon::find($attributes['my_pokemon_id']['third_my_pokemon_id'])->item_id,
            UserPokemon::find($attributes['my_pokemon_id']['fourth_my_pokemon_id'])->item_id,
            UserPokemon::find($attributes['my_pokemon_id']['fifth_my_pokemon_id'])->item_id,
            UserPokemon::find($attributes['my_pokemon_id']['sixth_my_pokemon_id'])->item_id
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return count(array_unique($this->itemsIds)) === self::PARTY_ITEM_NUMBER;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '同じアイテムを持ったポケモンが２匹以上います。';
    }
}
