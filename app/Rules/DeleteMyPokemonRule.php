<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Model\UserPokemon;

class DeleteMyPokemonRule implements Rule
{
    /**
     * [passes description]
     * @param  [type] $attribute [description]
     * @param  [type] $value     [description]
     * @return [type]            [description]
     */
    public function passes($attribute, $value)
    {
        $myPokemon = UserPokemon::find($value);
        $partyFirstCount = $myPokemon->firstUserPokemons()->count();
        $partySecondCount = $myPokemon->secondUserPokemons()->count();
        $partyThirdCount = $myPokemon->thirdUserPokemons()->count();
        $partyFourthCount = $myPokemon->fourthUserPokemons()->count();
        $partyFifthCount = $myPokemon->fifthUserPokemons()->count();
        $partySixthCount = $myPokemon->sixthUserPokemons()->count();
        $partySumCount = $partyFirstCount + $partySecondCount + $partyThirdCount + $partyFourthCount + $partyFifthCount + $partySixthCount;

        return $partySumCount === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'あなたのパーティーにこのポケモンが登録されています。削除できません。';
    }
}
