<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Model\User;

class DeleteUserRule implements Rule
{
    /**
     * [passes description]
     * @param  [type] $attribute [description]
     * @param  [type] $value     [description]
     * @return [type]            [description]
     */
    public function passes($attribute, $value)
    {
        $user = User::find($value);
        $userPokemonsCount = $user->userPokemons()->get()->count();
        $userPartiesCount = $user->parties()->get()->count();
        return $userPokemonsCount === 0 && $userPartiesCount === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '紐づく情報があるため削除できません。';
    }
}
