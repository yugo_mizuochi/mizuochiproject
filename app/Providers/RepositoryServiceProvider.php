<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // ホーム画面お知らせリポジトリパターン
        $this->app->bind(
            \App\Repositories\Notice\NoticeRepositoryInterface::class,
            \App\Repositories\Notice\NoticeReository::class
        );

        // マイポケモンリポジトリパターン
        $this->app->bind(
            \App\Repositories\MyPokemon\MyPokemonRepositoryInterface::class,
            \App\Repositories\MyPokemon\MyPokemonRepository::class
        );

        //マイパーティリポジトリパターン
        $this->app->bind(
            \App\Repositories\MyParty\MyPartyRepositoryInterface::class,
            \App\Repositories\MyParty\MyPartyRepository::class
        );

        //ポケモン検索リポジトリパターン
        $this->app->bind(
            \App\Repositories\UserPokemon\UserPokemonRepositoryInterface::class,
            \App\Repositories\UserPokemon\UserPokemonRepository::class
        );

        //お気に入りポケモンリポジトリパターン
        $this->app->bind(
            \App\Repositories\FavoriteMyPokemon\FavoriteMyPokemonRepositoryInterface::class,
            \App\Repositories\FavoriteMyPokemon\FavoriteMyPokemonRepository::class
        );

        //パーティ検索リポジトリパターン
        $this->app->bind(
            \App\Repositories\UserParty\UserPartyRepositoryInterface::class,
            \App\Repositories\UserParty\UserPartyRepository::class
        );

        //お気に入りパーティリポジトリパターン
        $this->app->bind(
            \App\Repositories\FavoriteMyParty\FavoriteMyPartyReositoryInterface::class,
            \App\Repositories\FavoriteMyParty\FavoriteMyPartyRepository::class
        );

        //お知らせ管理リポジトリパターン
        $this->app->bind(
            \App\Repositories\AdminNotice\AdminNoticeRepositoryInterface::class,
            \App\Repositories\AdminNotice\AdminNoticeRepository::class
        );

        //ユーザー管理リポジトリパターン実装
        $this->app->bind(
            \App\Repositories\AdminUser\AdminUserRepositoryInterface::class,
            \App\Repositories\AdminUser\AdminUserRepository::class
        );
    }
}
