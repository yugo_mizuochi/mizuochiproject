<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class UserType extends Enum
{
    const ADMIN = 'admin';
    const GENERAL = 'general';

    /**
     * ユーザータイプ判別
     *
     * @param string $userType
     * @return string
     */
    public static function judgeUserType(string $userType)
    {
        switch ($userType) {
            case self::ADMIN:
                return '管理者';
                break;
            
            default:
                return '一般';
                break;
        }
    }

    /**
     * ユーザータイプリスト取得
     *
     * @return array
     */
    public static function getUserTypes()
    {
        return [
            self::GENERAL => self::judgeUserType(self::GENERAL),
            self::ADMIN => self::judgeUserType(self::ADMIN)
        ];
    }
}
