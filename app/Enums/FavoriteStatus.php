<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class FavoriteStatus extends Enum
{
    const FAVORITE_STATUS = '追加';
    const RELEASE_STATUS = '解除';
}
