<?php

namespace App\UseCases\AdminUser;

use App\Consts\MessageConst;
use App\Repositories\AdminUser\AdminUserRepositoryInterface as AdminUserRepository;
use Illuminate\Http\Request;

final class AdminUserUseCase
{
	/** @var AdminUserRepository $adminUserRepository */
	private $adminUserRepository;

	/**
	 * @param AdminUserRepository $adminUserRepository
	 */
	public function __construct(AdminUserRepository $adminUserRepository)
	{
		$this->adminUserRepository = $adminUserRepository;
	}

	/**
	 * ユーザーリスト取得
	 *
	 * @return object
	 */
	public function getUsers()
	{
		return $this->adminUserRepository->getUsers();
	}

	/**
	 * ユーザータイプリスト取得
	 *
	 * @return void
	 */
	public function getUserTypes()
	{
		return $this->adminUserRepository->getUserTypes();
	}

	/**
	 * ユーザー新規登録
	 *
	 * @param Request $params
	 * @return string
	 */
	public function addUser(Request $params)
	{
		$addParams = [
			'user_name' => $params->input('user_name'),
			'user_email' => $params->input('user_email'),
			'user_password' => $params->input('user_password'),
			'user_type' => $params->input('user_type')
		];

		$user = $this->adminUserRepository->addUser($addParams);

		return $user->getName() . MessageConst::ADD_USER_MESSAGE;
	}

	/**
	 * 編集対象ユーザー情報取得
	 *
	 * @param integer $userId
	 * @return object
	 */
	public function getUser(int $userId)
	{
		return $this->adminUserRepository->getUser($userId);
	}

	/**
	 * ユーザー編集
	 *
	 * @param Request $params
	 * @param integer $userId
	 * @return string
	 */
	public function editUser(Request $params, int $userId)
	{
		$editParams = [
			'user_id' => $userId,
			'user_name' => $params->input('user_name'),
			'user_email' => $params->input('user_email'),
			'user_type' => $params->input('user_type')
		];

		$user = $this->adminUserRepository->editUser($editParams);

		return $user->getName() . MessageConst::EDIT_MESSAGE;
	}

	/**
	 * ユーザーとお気に入りポケモン、お気に入りパーティ削除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function deleteUser(Request $params)
	{
		$user = $this->adminUserRepository->deleteUser((int) $params->input('user_id'));

		return $user->getName() . MessageConst::DELETE_USER_MESSAGE;
	}
}