<?php

namespace App\UseCases\FavoriteMyPokemon;

use App\Consts\MessageConst;
use App\Repositories\FavoriteMyPokemon\FavoriteMyPokemonRepositoryInterface as FavoriteMyPokemonReository;
use Illuminate\Http\Request;

final class FavoriteMyPokemonUseCase
{
	/** @var FavoriteMyPokemonRepository $favoriteMyPokemonRepository */
	private $favoriteMyPokemonRepository;

	/**
	 * @param FavoriteMyPokemonReository $favoriteMyPokemonRepository
	 */
	public function __construct(FavoriteMyPokemonReository $favoriteMyPokemonRepository)
	{
		$this->favoriteMyPokemonRepository = $favoriteMyPokemonRepository;
	}

	/**
	 * お気に入りポケモン一覧取得
	 *
	 * @param integer $myId
	 * @return object
	 */
	public function getFavoriteMyPokemons(int $myId)
	{
		return $this->favoriteMyPokemonRepository->getFavoriteMyPokemons($myId);
	}

	/**
	 * お気に入りポケモンをマイポケモンに登録
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function addFavoriteToMyPokemon(Request $params, int $myId)
	{
		$favoriteId = (int) $params->input('favorite_id');
		$myPokemon = $this->favoriteMyPokemonRepository->addFavoriteToMyPokemon($favoriteId, $myId);
		return $myPokemon->getMyPokemonName() . MessageConst::ADD_MY_POKEMON_MESSAGE;
	}

	/**
	 * お気に入りポケモン解除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function releaseFavoriteMyPokemon(Request $params)
	{
		$favoriteId = (int) $params->input('favorite_id');
		$releaseMyPokemon = $this->favoriteMyPokemonRepository->releaseFavoriteMyPokemon($favoriteId);
		return $releaseMyPokemon->getUserPokemonName() . MessageConst::RELEASED_POKEMON_MESSAGE;
	}
}