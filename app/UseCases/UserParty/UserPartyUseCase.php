<?php

namespace App\UseCases\UserParty;

use App\Consts\MessageConst;
use App\Repositories\UserParty\UserPartyRepositoryInterface as UserPartyRepository;
use Illuminate\Http\Request;

final class UserPartyUseCase
{
	/** @var UserPartyRepository $userPartyRepository */
	private $userPartyRepository;

	/**
	 * @param UserPartyRepository $userPartyRepository
	 */
	public function __construct(UserPartyRepository $userPartyRepository)
	{
		$this->userPartyRepository = $userPartyRepository;
	}

	/**
	 * パーティ検索
	 *
	 * @param integer $myId
	 * @param integer $searchId
	 * @return void
	 */
	public function getUserParties(int $myId, int $searchId)
	{
		return $this->userPartyRepository->getUserParties($searchId, $myId);
	}

	/**
	 * ポケモン取得
	 *
	 * @return object
	 */
	public function getPokemons()
	{
		return $this->userPartyRepository->getPokemons();
	}

	/**
	 * 検索ID取得
	 *
	 * @param Request $params
	 * @return int
	 */
	public function getSearchId(Request $params)
	{
		$searchId = (int) $params->input('search_pokemon_id');

		if ($searchId === 0) {
			return $searchId;
		}

		return $this->userPartyRepository->getSearch($searchId)->getId();
	}

	/**
	 * パーティお気に入り追加
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function addFavoriteUserParty(Request $params, int $myId)
	{
		$userPartyId = (int) $params->input('user_party_id');
		$favoriteUserParty = $this->userPartyRepository->addFavoriteUserParty($userPartyId, $myId);
		return $favoriteUserParty->getUserPartyName() . MessageConst::FAVORITED_PARTY_MESSAGE;
	}

	/**
	 * パーティお気に入り解除
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function releaseFavoriteUserParty(Request $params, int $myId)
	{
		$userPartyId = (int) $params->input('user_party_id');
		$releaseUserParty = $this->userPartyRepository->releaseFavoriteUserParty($userPartyId, $myId);
		return $releaseUserParty->getUserPartyName() . MessageConst::RELEASED_PARTY_MESSAGE;
	}
}