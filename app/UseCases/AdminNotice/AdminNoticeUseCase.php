<?php
namespace App\UseCases\AdminNotice;

use App\Consts\MessageConst;
use App\Repositories\AdminNotice\AdminNoticeRepositoryInterface as AdminNoticeRepository;
use Illuminate\Http\Request;

final class AdminNoticeUseCase
{
	/** @var AdminNoticeRepository $adminNoticeRepository */
	private $adminNoticeRepository;

	/**
	 * @param AdminNoticeRepository $adminNoticeRepository
	 */
	public function __construct(AdminNoticeRepository $adminNoticeRepository)
	{
		$this->adminNoticeRepository = $adminNoticeRepository;
	}

    /**
	 * お知らせ一覧取得
	 *
	 * @return object
	 */
	public function getNotices()
	{
		return $this->adminNoticeRepository->getNotices();
	}

	/**
	 * お知らせ新規登録
	 *
	 * @param Request $params
	 * @return string
	 */
	public function addNotice(Request $params)
	{
		$addParams = [
			'notice_body' => $params->input('notice_body')
		];
		$notice = $this->adminNoticeRepository->addNotice($addParams);

		return 'お知らせ番号' . $notice->getId() . MessageConst::ADD_NOTICE_MESSAGE;
	}

	/**
	 * 編集お知らせ取得
	 *
	 * @param integer $noticeId
	 * @return Object
	 */
	public function getNotice(int $noticeId)
	{
		return $this->adminNoticeRepository->getNotice($noticeId);
	}

	/**
	 * お知らせ編集
	 *
	 * @param Request $params
	 * @param integer $noticeId
	 * @return string
	 */
	public function editNotice(Request $params, int $noticeId)
	{
		$editParams = [
			'notice_id' => $noticeId,
			'notice_body' => $params->input('notice_body')
		];
		$notice = $this->adminNoticeRepository->editNotice($editParams);

		return 'お知らせ番号' . $notice->getId() . MessageConst::EDIT_MESSAGE;
	}

	/**
	 * お知らせ削除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function deleteNotice(Request $params)
	{
		$notice = $this->adminNoticeRepository->deleteNotice((int) $params->input('notice_id'));

		return 'お知らせ番号' . $notice->getId() . MessageConst::DELETE_NOTICE_MESSAGE;
	}
}