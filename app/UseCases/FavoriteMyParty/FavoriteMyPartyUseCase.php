<?php

namespace App\UseCases\FavoriteMyParty;

use App\Consts\MessageConst;
use App\Repositories\FavoriteMyParty\FavoriteMyPartyReositoryInterface as FavoriteMyPartyRepository;
use Illuminate\Http\Request;
use App\Model\FavoriteParty;
use App\Model\Party;
use Illuminate\Support\Facades\DB;

final class FavoriteMyPartyUseCase
{
	/** @var FavoriteMyPartyRepository $favoriteMyPartyRepository */
	private $favoriteMyPartyRepository;

	/**
	 * @param FavoriteMyPartyRepository $favoriteMyPartyRepository
	 */
	public function __construct(FavoriteMyPartyRepository $favoriteMyPartyRepository)
	{
		$this->favoriteMyPartyRepository = $favoriteMyPartyRepository;
	}

	/**
	 * お気に入りパーティ一覧取得
	 *
	 * @param integer $myId
	 * @return object
	 */
	public function getFavoriteMyParties(int $myId)
	{
		return $this->favoriteMyPartyRepository->getFavoriteMyParties($myId);
	}

	/**
	 * お気に入りパーティをマイパーティに登録
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function addFavoriteToMyParty(Request $params, int $myId)
	{
		$favoriteId = (int) $params->input('favorite_party_id');
		$myParty = $this->favoriteMyPartyRepository->addFavoriteToMyParty($favoriteId, $myId);
		return $myParty->getName() . MessageConst::ADD_MY_PARTY_MESSAGE;
	}

	/**
	 * お気に入りパーティを解除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function releaseFavoriteMyParty(Request $params)
	{
		$favoriteId = (int) $params->input('favorite_party_id');
		$releaseMyParty = $this->favoriteMyPartyRepository->releaseFavoriteMyParty($favoriteId);
		return $releaseMyParty->getUserPartyName() . MessageConst::RELEASED_PARTY_MESSAGE;
	}
}