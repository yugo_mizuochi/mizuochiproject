<?php
namespace App\UseCases\Notice;

use App\Repositories\Notice\NoticeRepositoryInterface as NoticeRepository;

final class NoticeUseCase
{
	/** @var NoticeRepository $noticeRepository */
	private $noticeRepository;

	/**
	 * @param NoticeRepository $noticeRepository
	 */
	public function __construct(NoticeRepository $noticeRepository)
	{
		$this->noticeRepository = $noticeRepository;
	}

	/**
	 * お知らせ一覧取得
	 *
	 * @return object
	 */
	public function getNoticies()
	{
		return $this->noticeRepository->getNoticies();
	}
}