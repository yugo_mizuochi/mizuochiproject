<?php

namespace App\UseCases\MyPokemon;

use App\Model\UserPokemon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Consts\MessageConst;
use App\Repositories\MyPokemon\MyPokemonRepositoryInterface as MyPokemonRepository;

final class MyPokemonUseCase
{
	/** @var MyPokemonRepository $myPokemonRepository */
	private $myPokemonRepository;

	/**
	 * @param MyPokemonRepository $myPokemonRepository
	 */
	public function __construct(MyPokemonRepository $myPokemonRepository)
	{
		$this->myPokemonRepository = $myPokemonRepository;
	}

	/**
	 * マイポケモン一覧取得
	 *
	 * @param integer $myId
	 * @return object
	 */
	public function getMyPokemons(int $myId)
	{
		return $this->myPokemonRepository->getMyPokemons((int) $myId);
	}

	/**
	 * マイポケモン情報取得
	 *
	 * @param integer $myPokemonId
	 * @return object
	 */
	public function getMyPokemon(int $myPokemonId)
	{
		return $this->myPokemonRepository->getMyPokemon((int) $myPokemonId);
	}

	/**
	 * ポケモンマスタ一覧取得
	 *
	 * @return object
	 */
	public function getPokemons()
	{
		return $this->myPokemonRepository->getPokemons();
	}

	/**
	 * アイテムマスタ一覧取得
	 *
	 * @return object
	 */
	public function getItems()
	{
		return $this->myPokemonRepository->getItems();
	}

	/**
	 * マイポケモンに新規登録
	 *
	 * @param integer $myId
	 * @param Request $params
	 * @return string
	 */
	public function addMyPokemon(int $myId, Request $params)
	{
		$addParams = [
			'my_id' => $myId,
			'pokemon_id' => $params->input('pokemon_id'),
			'my_pokemon_name' => $params->input('my_pokemon_name'),
			'item_id' => $params->input('item_id'),
			'my_pokemon_memo' => $params->input('my_pokemon_memo')
		];

		$effortValues = $params->input('effort_values');
		foreach ($effortValues as $key => $effortValue) {
			$addParams[$key] = $effortValue;
		}

		$myPokemon = $this->myPokemonRepository->addMyPokemon($addParams);

		return $myPokemon->getMyPokemonName() . MessageConst::ADD_MY_POKEMON_MESSAGE;
	}

	/**
	 * マイポケモン編集
	 *
	 * @param Request $params
	 * @param integer $myPokemonId
	 * @param integer $myId
	 * @return string
	 */
	public function editMyPokemon(Request $params, int $myPokemonId, int $myId)
	{
		$editParams = [
			'my_id' => $myId,
			'my_pokemon_id' => $myPokemonId,
			'pokemon_id' => $params->input('pokemon_id'),
			'my_pokemon_name' => $params->input('my_pokemon_name'),
			'item_id' => $params->input('item_id'),
			'my_pokemon_memo' => $params->input('my_pokemon_memo')
		];

		$effortValues = $params->input('effort_values');
		foreach ($effortValues as $key => $effortValue) {
			$editParams[$key] = $effortValue;
		}

		$myPokemon = $this->myPokemonRepository->editMyPokemon($editParams);

		return $myPokemon->getMyPokemonName() . MessageConst::EDIT_MESSAGE;
	}

	/**
	 * マイポケモン削除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function deleteMyPokemon(Request $params)
	{
		$myPokemon = $this->myPokemonRepository->deleteMyPokemon((int) $params->input('my_pokemon_id'));

		return $myPokemon->getMyPokemonName() . MessageConst::DELETE_MY_POKEMON_MESSAGE;
	}
}