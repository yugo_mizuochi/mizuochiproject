<?php

namespace App\UseCases\UserPokemon;

use App\Consts\MessageConst;
use App\Repositories\UserPokemon\UserPokemonRepositoryInterface as UserPokemonRepository;
use Illuminate\Http\Request;

final class UserPokemonUseCase
{
	/** UserPokemonRepository $userPokemonRepository */
	private $userPokemonRepository;

	/**
	 * @param UserPokemonRepository $userPokemonRepository
	 */
	public function __construct(UserPokemonRepository $userPokemonRepository)
	{
		$this->userPokemonRepository = $userPokemonRepository;
	}
	
	/**
	 * ポケモン検索
	 *
	 * @param integer $pokemonId
	 * @param integer $myId
	 * @return object
	 */
	public function getUserPokemons(int $searchId, int $myId)
	{
		return $this->userPokemonRepository->getUserPokemons($searchId, $myId);
	}

	/**
	 * ポケモン取得
	 *
	 * @return object
	 */
	public function getPokemons()
	{
		return $this->userPokemonRepository->getPokemons();
	}

	/**
	 * 検索ID取得
	 *
	 * @param Request $params
	 * @return int
	 */
	public function getSearchId(Request $params)
	{
		$searchId = (int) $params->input('search_pokemon_id');

		if ($searchId === 0) {
			return $searchId;
		}

		return $this->userPokemonRepository->getSearch($searchId)->getId();
	}

	/**
	 * ポケモンお気に入り追加
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function addFavoriteUserPokemon(Request $params, int $myId)
	{
		$userPokemonId = (int) $params->input('user_pokemon_id');
		$favoriteUserPokemon = $this->userPokemonRepository->addFavorite($userPokemonId, $myId);
		return $favoriteUserPokemon->getUserPokemonName() . MessageConst::FAVORITED_POKEMON_MESSAGE;
	}

	/**
	 * ポケモンお気に入り解除
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function releaseFavoriteUserPokemon(Request $params, int $myId)
	{
		$userPokemonId = (int) $params->input('user_pokemon_id');
		$releaseUserPokemon = $this->userPokemonRepository->releaseFavorite($userPokemonId, $myId);
		return $releaseUserPokemon->getUserPokemonName() . MessageConst::RELEASED_POKEMON_MESSAGE;
	}
}