<?php

namespace App\UseCases\MyParty;

use App\Repositories\MyParty\MyPartyRepositoryInterface as MyPartyRepository;
use App\Consts\MessageConst;
use Illuminate\Http\Request;

final class MyPartyUseCase
{
	/** @var MyPartyRepository $myPartyRepository */
	private $myPartyRepository;

	/**
	 * @param MyPartyRepository $myPartyRepository
	 */
	public function __construct(MyPartyRepository $myPartyRepository)
	{
		$this->myPartyRepository = $myPartyRepository;
	}

	/**
	 * マイパーティ一覧取得
	 *
	 * @param integer $myId
	 * @return object
	 */
	public function getMyParties(int $myId)
	{
		return $this->myPartyRepository->getMyParties((int) $myId);
	}

	/**
	 * マイポケモンリスト取得
	 *
	 * @param integer $myId
	 * @return object
	 */
	public function getMyPokemons(int $myId)
	{
		return $this->myPartyRepository->getMyPokemons((int) $myId);
	}

	/**
	 * マイパーティ新規登録
	 *
	 * @param Request $params
	 * @param integer $myId
	 * @return string
	 */
	public function addMyParty(Request $params, int $myId)
	{
		$addParams = [
			'my_id' => $myId,
			'my_party_name' => $params->input('my_party_name'),
			'my_party_memo' => $params->input('my_party_memo')
		];

		$myPokemonsIds = $params->input('my_pokemon_id');
		foreach ($myPokemonsIds as $key => $myPokemonId) {
			$addParams[$key] = $myPokemonId;
		}
		
		$myParty = $this->myPartyRepository->addMyParty($addParams);

		return $myParty->getName() . MessageConst::ADD_MY_PARTY_MESSAGE;
	}

	/**
	 * 編集対象マイパーティ取得
	 *
	 * @param integer $myPartyId
	 * @return void
	 */
	public function getMyParty(int $myPartyId)
	{
		return $this->myPartyRepository->getMyParty($myPartyId);
	}

	/**
	 * マイパーティ編集
	 *
	 * @param Request $params
	 * @param integer $myPartyId
	 * @param integer $myId
	 * @return string
	 */
	public function editMyParty(Request $params, int $myPartyId, int $myId)
	{
		$editParams = [
			'my_id' => $myId,
			'my_party_id' => $myPartyId,
			'my_party_name' => $params->input('my_party_name'),
			'my_party_memo' => $params->input('my_party_memo')
		];

		$myPokemonsIds = $params->input('my_pokemon_id');
		foreach ($myPokemonsIds as $key => $myPokemonId) {
			$editParams[$key] = $myPokemonId;
		}

		$myParty = $this->myPartyRepository->editMyParty($editParams);

		return $myParty->getName() . MessageConst::EDIT_MESSAGE;
	}

	/**
	 * マイパーティ削除
	 *
	 * @param Request $params
	 * @return string
	 */
	public function deleteMyParty(Request $params)
	{
		$myParty = $this->myPartyRepository->deleteMyParty((int) $params->input('my_party_id'));

		return $myParty->getName() . MessageConst::DELETE_MY_PARTY_MESSAGE;
	}
}