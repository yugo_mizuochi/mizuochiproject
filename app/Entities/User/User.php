<?php
namespace App\Entities\User;

final class User
{
    /** @var int $id */
    private $id;

    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /** @var string $password */
    private $password;

    /** @var string $userType */
    private $userType;

    /**
     * @param integer $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $userType
     */
    public function __construct(
        int $id,
        string $name,
        string $email,
        string $password,
        string $userType
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->userType = $userType;
    }

    /**
     * ユーザーID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ユーザーネーム取得
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * ユーザーメールアドレス取得
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * ユーザーパスワード取得
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * ユーザー権限取得
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }
}