<?php
namespace App\Entities\User;

use App\Entities\User\User;
use Illuminate\Support\HtmlString;

final class UserList implements \IteratorAggregate
{
    /** @var ArrayObject $userList */
    private $userList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->userList = new \ArrayObject();
    }

    /**
     * ユーザーリスト生成
     *
     * @param User $user
     */
    public function add(User $user)
    {
        $this->userList[] = $user;
    }

    /**
     * ユーザーリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->userList->getIterator();
    }

    /**
     * ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}