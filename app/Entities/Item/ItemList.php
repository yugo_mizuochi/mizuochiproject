<?php
namespace App\Entities\Item;

use App\Entities\Item\Item;

final class ItemList implements \IteratorAggregate
{
    /** @var ArrayObject $itemList */
    private $itemList;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->itemList = new \ArrayObject();
    }

    /**
     * アイテムリスト生成
     *
     * @param Item $item
     */
    public function add(Item $item)
    {
        $this->itemList[] = $item;
    }

    /**
     * アイテムリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->itemList->getIterator();
    }
}