<?php
namespace App\Entities\Item;

final class Item
{
    /** @var int $id */
    private $id;

    /** @var $name */
    private $name;

    /**
     * @param integer $id
     * @param string $name
     */
    public function __construct(
        int $id,
        string $name
    )
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * アイテムマスタID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * アイテムマスタネーム取得
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}