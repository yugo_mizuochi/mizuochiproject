<?php
namespace App\Entities\FavoriteMyParty;

use App\Entities\UserParty\UserParty;

final class FavoriteMyParty
{
    /** @var int $id */
    private $id;

    /** @var UserParty $userParty */
    private $userParty;

    /**
     * @param integer $id
     * @param UserParty $userParty
     */
    public function __construct(
        int $id,
        UserParty $userParty
    )
    {
        $this->id = $id;
        $this->userParty = $userParty;
    }

    /**
     * お気に入りパーティID取得
     *
     * @return void
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * お気に入りパーティ情報取得
     *
     * @return UserParty
     */
    public function getUserParty()
    {
        return $this->userParty;
    }
}