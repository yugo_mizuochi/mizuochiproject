<?php
namespace App\Entities\FavoriteMyParty;

use App\Entities\FavoriteMyParty\FavoriteMyParty;
use Illuminate\Support\HtmlString;

final class FavoriteMyPartyList implements \IteratorAggregate
{
    /** @var ArrayObject $favoriteMyPartyList */
    private $favoriteMyPartyList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->favoriteMyPartyList = new \ArrayObject();
    }

    /**
     * お気に入りパーティリスト生成
     *
     * @param FavoriteMyParty $favoriteMyParty
     */
    public function add(FavoriteMyParty $favoriteMyParty)
    {
        $this->favoriteMyPartyList[] = $favoriteMyParty;
    }

    /**
     * お気に入りパーティリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->favoriteMyPartyList->getIterator();
    }

    /**
     * お気に入りパーティ一覧ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * お気に入りパーティ一覧ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}