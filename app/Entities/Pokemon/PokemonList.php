<?php
namespace App\Entities\Pokemon;

use App\Entities\Pokemon\Pokemon;

final class PokemonList implements \IteratorAggregate
{
    /** @var ArrayObject $pokemonList */
    private $pokemonList;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->pokemonList = new \ArrayObject();
    }

    /**
     * ポケモンマスタリスト生成
     *
     * @param Pokemon $pokemon
     * @return void
     */
    public function add(Pokemon $pokemon)
    {
        $this->pokemonList[] = $pokemon;
    }

    /**
     * ポケモンマスタリスト取得
     *
     * @return mixed
     */
    public function getIterator()
    {
        return $this->pokemonList->getIterator();
    }
}