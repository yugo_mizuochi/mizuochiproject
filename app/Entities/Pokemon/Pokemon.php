<?php
namespace App\Entities\Pokemon;

final class Pokemon
{
    /** @var int $id */
    private $id;

    /** @var string $name */
    private $name;

    /**
     * ポケモンマスタ情報セット
     *
     * @param integer $id
     * @param string $name
     */
    public function __construct(
        int $id,
        string $name
    )
    {
        $this->id = $id;
        $this->name = $name;        
    }

    /**
     * ポケモンマスタID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ポケモンマスタ名取得
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}