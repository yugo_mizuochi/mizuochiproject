<?php
namespace App\Entities\ReleaseMyParty;

final class ReleaseMyParty
{
    /** @var string $userPartyName */
    private $userPartyName;

    /**
     * @param string $userPartyName
     */
    public function __construct(string $userPartyName)
    {
        $this->userPartyName = $userPartyName;
    }

    /**
     * 解除したパーティ名取得
     *
     * @return string
     */
    public function getUserPartyName()
    {
        return $this->userPartyName;
    }
}