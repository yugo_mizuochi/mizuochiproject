<?php
namespace App\Entities\MyParty;

use App\Entities\MyParty\MyParty;
use Illuminate\Support\HtmlString;

final class MyPartyList implements \IteratorAggregate
{
    /** @var ArrayObject $myPartyList */
    private $myPartyList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /** @var int $total */
    private $total;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->myPartyList = new \ArrayObject();
    }

    /**
     * マイパーティリスト生成
     *
     * @param MyParty $myParty
     */
    public function add(MyParty $myParty)
    {
        $this->myPartyList[] = $myParty;
    }

    /**
     * マイパーティリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->myPartyList->getIterator();
    }

    /**
     * マイパーティ総数セット
     *
     * @param integer $total
     */
    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    /**
     * マイパーティ総数取得
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}