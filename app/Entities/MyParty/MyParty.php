<?php
namespace App\Entities\MyParty;

use App\Entities\MyPokemon\MyPokemon;
use App\Entities\User\User;

final class MyParty
{
    /** @var int $id */
    private $id;

    /** @var User $my */
    private $my;

    /** @var string $name */
    private $name;

    /** @var MyPokemon $firstMyPokemon */
    private $firstMyPokemon;

    /** @var MyPokemon $secondMyPokemon */
    private $secondMyPokemon;

    /** @var MyPokemon $thirdMyPokemon */
    private $thirdMyPokemon;

    /** @var MyPokemon $fourthMyPokemon */
    private $fourthMyPokemon;

    /** @var MyPokemon $fifthMyPokemon */
    private $fifthMyPokemon;

    /** @var MyPokemon $sixthMyPokemon */
    private $sixthMyPokemon;

    /** @var string $memo */
    private $memo;

    /**
     * @param integer $id
     * @param User $my
     * @param string $name
     * @param MyPokemon $firstMyPokemon
     * @param MyPokemon $secondMyPokemon
     * @param MyPokemon $thirdMyPokemon
     * @param MyPokemon $fourthMyPokemon
     * @param MyPokemon $fifthMyPokemon
     * @param MyPokemon $sixthMyPokemon
     * @param string $memo
     */
    public function __construct(
        int $id,
        User $my,
        string $name,
        MyPokemon $firstMyPokemon,
        MyPokemon $secondMyPokemon,
        MyPokemon $thirdMyPokemon,
        MyPokemon $fourthMyPokemon,
        MyPokemon $fifthMyPokemon,
        MyPokemon $sixthMyPokemon,
        string $memo
    )
    {
        $this->id = $id;
        $this->my = $my;
        $this->name = $name;
        $this->firstMyPokemon = $firstMyPokemon;
        $this->secondMyPokemon = $secondMyPokemon;
        $this->thirdMyPokemon = $thirdMyPokemon;
        $this->fourthMyPokemon = $fourthMyPokemon;
        $this->fifthMyPokemon = $fifthMyPokemon;
        $this->sixthMyPokemon = $sixthMyPokemon;
        $this->memo = $memo;
    }

    /**
     * マイパーティID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * マイパーティユーザー取得
     *
     * @return User
     */
    public function getMy()
    {
        return $this->my;
    }

    /**
     * マイパーティ名取得
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * マイパーティポケモン1取得
     *
     * @return MyPokemon
     */
    public function getFirstMyPokemon()
    {
        return $this->firstMyPokemon;
    }

    /**
     * マイパーティポケモン2取得
     *
     * @return MyPokemon
     */
    public function getSecondMyPokemon()
    {
        return $this->secondMyPokemon;
    }

    /**
     * マイパーティポケモン3取得
     *
     * @return MyPokemon
     */
    public function getThirdMyPokemon()
    {
        return $this->thirdMyPokemon;
    }

    /**
     * マイパーティポケモン4取得
     *
     * @return MyPokemon
     */
    public function getFourthMyPokemon()
    {
        return $this->fourthMyPokemon;
    }

    /**
     * マイパーティポケモン5取得
     *
     * @return Mypokemon
     */
    public function getFifthMyPokemon()
    {
        return $this->fifthMyPokemon;
    }

    /**
     * マイパーティポケモン6取得
     *
     * @return MyPokemon
     */
    public function getSixthMyPokemon()
    {
        return $this->sixthMyPokemon;
    }

    /**
     * マイパーティメモ取得
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }
}