<?php
namespace App\Entities\ReleaseMyPokemon;

final class ReleaseMyPokemon
{
    /** @var string $userPokemonName */
    private $userPokemonName;

    /**
     * @param string $userPokemonName
     */
    public function __construct(string $userPokemonName)
    {
        $this->userPokemonName = $userPokemonName;
    }

    /**
     * 解除したポケモンニックネーム取得
     *
     * @return string
     */
    public function getUserPokemonName()
    {
        return $this->userPokemonName;
    }
}