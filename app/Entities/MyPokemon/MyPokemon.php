<?php
namespace App\Entities\MyPokemon;

use App\Entities\Pokemon\Pokemon;
use App\Entities\Item\Item;
use App\Entities\User\User;

final class MyPokemon
{
    /** @var int $id */
    private $id;

    /** @var Pokemon $pokemon */
    private $pokemon;

    /** @var Item $item */
    private $item;

    /** @var User $my */
    private $my;

    /** @var string $myPokemonName */
    private $myPokemonName;

    /** @var int $hp */
    private $hp;

    /** @var int $attack */
    private $attack;

    /** @var int $defense */
    private $defense;

    /** @var int $specialAttack */
    private $specialAttack;

    /** @var string $specialDefense */
    private $specialDefense;

    /** @var int $agility */
    private $agility;

    /** @var string $memo */
    private $memo;

    /**
     * マイポケモン情報セット
     *
     * @param integer $id
     * @param Pokemon $pokemon
     * @param Item $item
     * @param User $my
     * @param string $myPokemonName
     * @param integer $hp
     * @param integer $attack
     * @param integer $defense
     * @param integer $specialAttack
     * @param integer $specialDefense
     * @param integer $agility
     * @param string $memo
     */
    public function __construct(
        int $id,
        Pokemon $pokemon,
        Item $item,
        User $my,
        string $myPokemonName,
        int $hp,
        int $attack,
        int $defense,
        int $specialAttack,
        int $specialDefense,
        int $agility,
        string $memo
    )
    {
        $this->id = $id;
        $this->pokemon = $pokemon;
        $this->item = $item;
        $this->my = $my;
        $this->myPokemonName = $myPokemonName;
        $this->hp = $hp;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->specialAttack = $specialAttack;
        $this->specialDefense = $specialDefense;
        $this->agility = $agility;
        $this->memo = $memo;
    }

    /**
     * マイポケモンID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ポケモンマスタ取得
     *
     * @return Pokemon
     */
    public function getPokemon()
    {
        return $this->pokemon;
    }

    /**
     * アイテムマスタ取得
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * ユーザー情報取得
     *
     * @return User
     */
    public function getMy()
    {
        return $this->my;
    }

    /**
     * マイポケモンニックネーム取得
     *
     * @return string
     */
    public function getMyPokemonName()
    {
        return $this->myPokemonName;
    }

    /**
     * マイポケモンHP努力値取得
     *
     * @return int
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * マイポケモン攻撃努力値取得
     *
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * マイポケモン防御努力値取得
     *
     * @return int
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     * マイポケモン特攻努力値取得
     *
     * @return int
     */
    public function getSpecialAttack()
    {
        return $this->specialAttack;
    }

    /**
     * マイポケモン特防努力値取得
     *
     * @return int
     */
    public function getSpecialDefense()
    {
        return $this->specialDefense;
    }

    /**
     * マイポケモン素早さ努力値取得
     *
     * @return int
     */
    public function getAgility()
    {
        return $this->agility;
    }
    
    /**
     * マイポケモンメモ取得
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }
}