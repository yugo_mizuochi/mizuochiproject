<?php
namespace App\Entities\MyPokemon;

use App\Entities\MyPokemon\MyPokemon;
use Illuminate\Support\HtmlString;

final class MyPokemonList implements \IteratorAggregate
{
    /** @var ArrayObject $myPokemonList */
    private $myPokemonList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /** @var int $total */
    private $total;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->myPokemonList = new \ArrayObject();
    }

    /**
     * マイポケモンリスト生成
     *
     * @param MyPokemon $myPokemon
     */
    public function add(MyPokemon $myPokemon)
    {
        $this->myPokemonList[] = $myPokemon;
    }

    /**
     * マイポケモンリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->myPokemonList->getIterator();
    }

    /**
     * マイポケモン総数セット
     *
     * @param integer $total
     */
    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    /**
     * マイポケモン総数取得
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}