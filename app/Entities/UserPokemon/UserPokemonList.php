<?php
namespace App\Entities\UserPokemon;

use App\Entities\UserPokemon\UserPokemon;
use Illuminate\Support\HtmlString;

final class UserPokemonList implements \IteratorAggregate
{
    /** @var ArrayObject $userPokemonList */
    private $userPokemonList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /** @var int $total */
    private $total;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->userPokemonList = new \ArrayObject();
    }

    /**
     * ポケモン検索結果リスト生成
     * 
     * @param UserPokemon $userPokemon
     */
    public function add(UserPokemon $userPokemon)
    {
        $this->userPokemonList[] = $userPokemon;
    }

    /**
     * ポケモン検索結果リスト取得
     *
     * @return mixed
     */
    public function getIterator()
    {
        return $this->userPokemonList->getIterator();
    }

    /**
     * ポケモン検索結果総数セット
     *
     * @param integer $total
     */
    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    /**
     * ポケモン検索結果総数取得
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * ポケモン検索一覧ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * ポケモン検索結果一覧ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}