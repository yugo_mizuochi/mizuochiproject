<?php
namespace App\Entities\UserPokemon;

use App\Entities\Pokemon\Pokemon;
use App\Entities\Item\Item;
use App\Entities\User\User;

final class UserPokemon
{
    /** @var int $id */
    private $id;

    /** @var Pokemon $pokemon */
    private $pokemon;

    /** @var Item $item */
    private $item;

    /** @var User $user */
    private $user;

    /** @var string $userPokemonName */
    private $userPokemonName;

    /** @var int $hp */
    private $hp;

    /** @var int $attack */
    private $attack;

    /** @var int $defense */
    private $defense;

    /** @var int $specialAttack */
    private $specialAttack;

    /** @var string $specialDefense */
    private $specialDefense;

    /** @var int $agility */
    private $agility;

    /** @var string $memo */
    private $memo;

    /** @var string $favoriteStatus */
    private $favoriteStatus;

    /** @var string $modalButtonClass */
    private $modalButtonClass;

    /** @var string $modalName */
    private $modalName;

    /**
     * 他ユーザーのポケモン情報セット
     *
     * @param integer $id
     * @param Pokemon $pokemon
     * @param Item $item
     * @param User $user
     * @param string $userPokemonName
     * @param integer $hp
     * @param integer $attack
     * @param integer $defense
     * @param integer $specialAttack
     * @param integer $specialDefense
     * @param integer $agility
     * @param string $memo
     * @param string $favoriteStatus
     * @param string $modalButtonClass
     * @param string $modalName
     */
    public function __construct(
        int $id,
        Pokemon $pokemon,
        Item $item,
        User $user,
        string $userPokemonName,
        int $hp,
        int $attack,
        int $defense,
        int $specialAttack,
        int $specialDefense,
        int $agility,
        string $memo,
        string $favoriteStatus,
        string $modalButtonClass,
        string $modalName
    )
    {
        $this->id = $id;
        $this->pokemon = $pokemon;
        $this->item = $item;
        $this->user = $user;
        $this->userPokemonName = $userPokemonName;
        $this->hp = $hp;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->specialAttack = $specialAttack;
        $this->specialDefense = $specialDefense;
        $this->agility = $agility;
        $this->memo = $memo;
        $this->favoriteStatus = $favoriteStatus;
        $this->modalButtonClass = $modalButtonClass;
        $this->modalName = $modalName;
    }

    /**
     * 他ユーザー登録ポケモンID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 他ユーザーポケモンマスタ情報取得
     *
     * @return Pokemon
     */
    public function getPokemon()
    {
        return $this->pokemon;
    }

    /**
     * 他ユーザーアイテムマスタ情報取得
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * 他ユーザー情報取得
     *
     * @return void
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 他ユーザー登録ポケモンニックネーム取得
     *
     * @return string
     */
    public function getUserPokemonName()
    {
        return $this->userPokemonName;
    }

    /**
     * 他ユーザー登録ポケモンHP努力値取得
     *
     * @return int
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * 他ユーザー登録ポケモン攻撃努力値取得
     *
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * 他ユーザー登録ポケモン防御努力値取得
     *
     * @return int
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     * 他ユーザー登録ポケモン特攻努力値取得
     *
     * @return int
     */
    public function getSpecialAttack()
    {
        return $this->specialAttack;
    }

    /**
     * 他ユーザー登録ポケモン特防努力値取得
     *
     * @return int
     */
    public function getSpecialDefense()
    {
        return $this->specialDefense;
    }

    /**
     * 他ユーザー登録ポケモン素早さ努力値取得
     *
     * @return int
     */
    public function getAgility()
    {
        return $this->agility;
    }
    
    /**
     * 他ユーザー登録ポケモンメモ取得
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * 他ユーザー登録ポケモンお気に入りステータス取得
     *
     * @return string
     */
    public function getFavoriteStatus()
    {
        return $this->favoriteStatus;
    }

    /**
     * 他ユーザー登録ポケモンモーダルボタンクラス名取得
     *
     * @return string
     */
    public function getModalButtonClass()
    {
        return $this->modalButtonClass;
    }

    /**
     * 他ユーザー登録ポケモンモーダル名取得
     *
     * @return string
     */
    public function getModalName()
    {
        return $this->modalName;
    }
}