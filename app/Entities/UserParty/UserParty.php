<?php
namespace App\Entities\UserParty;

use App\Entities\User\User;
use App\Entities\UserPokemon\UserPokemon;

final class UserParty
{
    /** @var int $id */
    private $id;

    /** @var User $user */
    private $user;

    /** @var string $userPartyName */
    private $userPartyName;

    /** @var UserPokemon $firstUserPokemon */
    private $firstUserPokemon;

    /** @var UserPokemon $secondUserPokemon */
    private $secondUserPokemon;

    /** @var UserPokemon $thirdUserPokemon */
    private $thirdUserPokemon;

    /** @var UserPokemon $fourthUserPokemon */
    private $fourthUserPokemon;

    /** @var UserPokemon $fifthUserPokemon */
    private $fifthUserPokemon;

    /** @var UserPokemon $sixthUserPokemon */
    private $sixthUserPokemon;

    /** @var string $memo */
    private $memo;

    /** @var string $favoriteStatus */
    private $favoriteStatus;

    /** @var string $modalButtonClass */
    private $modalButtonClass;

    /** @var string $modalName */
    private $modalName;

    /**
     * 他ユーザーのパーティ情報セット
     *
     * @param integer $id
     * @param User $user
     * @param string $userPartyName
     * @param UserPokemon $firstUserPokemon
     * @param UserPokemon $secondUserPokemon
     * @param UserPokemon $thirdUserPokemon
     * @param UserPokemon $fourthUserPokemon
     * @param UserPokemon $fifthUserPokemon
     * @param UserPokemon $sixthUserPokemon
     * @param string $memo
     * @param string $favoriteStatus
     * @param string $modalButtonClass
     * @param string $modalName
     */
    public function __construct(
        int $id,
        User $user,
        string $userPartyName,
        UserPokemon $firstUserPokemon,
        UserPokemon $secondUserPokemon,
        UserPokemon $thirdUserPokemon,
        UserPokemon $fourthUserPokemon,
        UserPokemon $fifthUserPokemon,
        UserPokemon $sixthUserPokemon,
        string $memo,
        string $favoriteStatus,
        string $modalButtonClass,
        string $modalName
    )
    {
        $this->id = $id;
        $this->user = $user;
        $this->userPartyName = $userPartyName;
        $this->firstUserPokemon = $firstUserPokemon;
        $this->secondUserPokemon = $secondUserPokemon;
        $this->thirdUserPokemon = $thirdUserPokemon;
        $this->fourthUserPokemon = $fourthUserPokemon;
        $this->fifthUserPokemon = $fifthUserPokemon;
        $this->sixthUserPokemon = $sixthUserPokemon;
        $this->memo = $memo;
        $this->favoriteStatus = $favoriteStatus;
        $this->modalButtonClass = $modalButtonClass;
        $this->modalName = $modalName;
    }

    /**
     * 他ユーザーパーティID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ユーザー情報取得
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 他ユーザーパーティ名取得
     *
     * @return string
     */
    public function getUserPartyName()
    {
        return $this->userPartyName;
    }

    /**
     * 他ユーザーポケモン1情報取得
     *
     * @return UserPokemon
     */
    public function getFirstUserPokemon()
    {
        return $this->firstUserPokemon;
    }

    /**
     * 他ユーザーポケモン2情報取得
     *
     * @return UserPokemon
     */
    public function getSecondUserPokemon()
    {
        return $this->secondUserPokemon;
    }

    /**
     * 他ユーザーポケモン3情報取得
     *
     * @return UserPokemon
     */
    public function getThirdUserPokemon()
    {
        return $this->thirdUserPokemon;
    }

    /**
     * 他ユーザーポケモン4情報取得
     *
     * @return UserPokemon
     */
    public function getFourthUserPokemon()
    {
        return $this->fourthUserPokemon;
    }

    /**
     * 他ユーザーポケモン5情報取得
     *
     * @return UserPokemon
     */
    public function getFifthUserPokemon()
    {
        return $this->fifthUserPokemon;
    }

    /**
     * 他ユーザーポケモン6情報取得
     *
     * @return UserPokemon
     */
    public function getSixthUserPokemon()
    {
        return $this->sixthUserPokemon;
    }

    /**
     * 他ユーザーパーティメモ取得
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * 他ユーザーパーティお気に入りステータス取得
     *
     * @return string
     */
    public function getFavoriteStatus()
    {
        return $this->favoriteStatus;
    }

    /**
     * 他ユーザーパーティモーダルボタンクラス名取得
     *
     * @return string
     */
    public function getModalButtonClass()
    {
        return $this->modalButtonClass;
    }

    /**
     * 他ユーザーパーティモーダル名取得
     *
     * @return string
     */
    public function getModalName()
    {
        return $this->modalName;
    }
}