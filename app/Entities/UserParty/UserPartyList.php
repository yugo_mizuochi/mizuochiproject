<?php
namespace App\Entities\UserParty;

use App\Entities\UserParty\UserParty;
use Illuminate\Support\HtmlString;

final class UserPartyList implements \IteratorAggregate
{
    /** @var ArrayObject $userPartyList */
    private $userPartyList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /** @var int $total */
    private $total;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->userPartyList = new \ArrayObject();
    }

    /**
     * パーティ検索結果リスト生成
     *
     * @param UserParty $userParty
     */
    public function add(UserParty $userParty)
    {
        $this->userPartyList[] = $userParty;
    }

    /**
     * パーティ検索結果取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->userPartyList->getIterator();
    }

    /**
     * パーティ検索結果総数セット
     *
     * @param integer $total
     */
    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    /**
     * パーティ検索結果総数取得
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * パーティ検索一覧ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * パーティ検索結果一覧ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}