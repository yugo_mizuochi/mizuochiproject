<?php
namespace App\Entities\Notice;

use App\Entities\Notice\Notice;
use Illuminate\Support\HtmlString;

final class NoticeList implements \IteratorAggregate
{
    /** @var ArrayObject $noticeList */
    private $noticeList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->noticeList = new \ArrayObject();
    }

    /**
     * お知らせリスト生成
     *
     * @param Notice $notice
     */
    public function add(Notice $notice)
    {
        $this->noticeList[] = $notice;
    }

    /**
     * お知らせリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->noticeList->getIterator();
    }

    /**
     * お知らせ一覧ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setLink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * お知らせ一覧一覧ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}