<?php
namespace App\Entities\Notice;

final class Notice
{
    /** @var int $id */
    private $id;

    /** @var string $body */
    private $body;

    /** string $date */
    private $date;

    /**
     * @param integer $id
     * @param string $body
     * @param string $date
     */
    public function __construct(
        int $id,
        string $body,
        string $date
    )
    {
        $this->id = $id;
        $this->body = $body;
        $this->date = $date;
    }

    /**
     * お知らせID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * お知らせ本文取得
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * お知らせ生成日取得
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
}