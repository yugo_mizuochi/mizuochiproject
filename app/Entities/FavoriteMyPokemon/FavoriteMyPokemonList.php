<?php
namespace App\Entities\FavoriteMyPokemon;

use App\Entities\FavoriteMyPokemon\FavoriteMyPokemon;
use Illuminate\Support\HtmlString;

final class FavoriteMyPokemonList implements \IteratorAggregate
{
    /** @var ArrayObject $favoriteMyPokemonList */
    private $favoriteMyPokemonList;

    /** @var HtmlString $pageLink */
    private $pageLink;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->favoriteMyPokemonList = new \ArrayObject();
    }

    public function add(FavoriteMyPokemon $favoriteMyPokemon)
    {
        $this->favoriteMyPokemonList[] = $favoriteMyPokemon;
    }

    /**
     * お気に入りポケモンリスト取得
     *
     * @return $this
     */
    public function getIterator()
    {
        return $this->favoriteMyPokemonList->getIterator();
    }

    /**
     * お気に入りポケモン一覧ページリンクセット
     *
     * @param HtmlString $link
     */
    public function setlink(HtmlString $link)
    {
        $this->pageLink = $link;
    }

    /**
     * お気に入りポケモン一覧ページリンク取得
     *
     * @return HtmlString
     */
    public function getLink()
    {
        return $this->pageLink;
    }
}