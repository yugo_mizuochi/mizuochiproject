<?php
namespace App\Entities\FavoriteMyPokemon;

use App\Entities\UserPokemon\UserPokemon;

final class FavoriteMyPokemon
{
    /** @var int $id */
    private $id;

    /** @var UserPokemon $userPokemon */
    private $userPokemon;

    /**
     * お気に入りポケモン情報セット
     *
     * @param integer $id
     * @param UserPokemon $userPokemon
     */
    public function __construct(
        int $id,
        UserPokemon $userPokemon
    )
    {
        $this->id = $id;
        $this->userPokemon = $userPokemon;
    }

    /**
     * お気に入りID取得
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * お気に入りポケモン情報取得
     *
     * @return UserPokemon
     */
    public function getUserPokemon()
    {
        return $this->userPokemon;
    }
}