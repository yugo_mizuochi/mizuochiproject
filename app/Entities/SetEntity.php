<?php
namespace App\Entities;

use App\Consts\HtmlAttributeConst;
use App\Consts\ModalNameConst;
use App\Enums\FavoriteStatus;
use App\Enums\UserType;
use App\Entities\FavoriteMyPokemon\FavoriteMyPokemon;
use App\Entities\Item\Item;
use App\Entities\MyParty\MyParty;
use App\Entities\MyPokemon\MyPokemon;
use App\Entities\Notice\Notice;
use App\Entities\Pokemon\Pokemon;
use App\Entities\ReleaseMyPokemon\ReleaseMyPokemon;
use App\Entities\User\User;
use App\Entities\UserPokemon\UserPokemon;
use App\Entities\UserParty\UserParty;
use App\Entities\FavoriteMyParty\FavoriteMyParty;
use App\Entities\ReleaseMyParty\ReleaseMyParty;
use App\Model\User as UserModel;
use App\Model\UserPokemon as UserPokemonModel;
use App\Model\Pokemon as PokemonModel;
use App\Model\Item as ItemModel;
use App\Model\Notice as NoticeModel;
use App\Model\Party as PartyModel;
use App\Model\Favorite as FavoriteModel;
use App\Model\FavoriteParty as FavoritePartyModel;

final class SetEntity
{
    /**
     * マイポケモンEntity生成
     *
     * @param UserPokemonModel $myPokemon
     * @return MyPokemon
     */
    public static function setMyPokemon(UserPokemonModel $myPokemon)
    {
        return new MyPokemon(
            (int) $myPokemon->id,
            self::setPokemon(PokemonModel::find($myPokemon->pokemon->id)),
            self::setItem(ItemModel::find($myPokemon->item->id)),
            self::setUser(UserModel::find($myPokemon->user_id)),
            $myPokemon->pokemon_name,
            (int) $myPokemon->hp,
            (int) $myPokemon->attack,
            (int) $myPokemon->defense,
            (int) $myPokemon->special_attack,
            (int) $myPokemon->special_defense,
            (int) $myPokemon->agility,
            $myPokemon->memo
        );
    }

    /**
     * マイパーティEntity生成
     *
     * @param PartyModel $myParty
     * @return MyParty
     */
    public static function setMyParty(PartyModel $myParty)
    {
        return new MyParty(
            (int) $myParty->id,
            self::setUser(UserModel::find($myParty->user_id)),
            $myParty->name,
            self::setMyPokemon(UserPokemonModel::find($myParty->first_user_pokemon_id)),
            self::setMyPokemon(UserPokemonModel::find($myParty->second_user_pokemon_id)),
            self::setMyPokemon(UserPokemonModel::find($myParty->third_user_pokemon_id)),
            self::setMyPokemon(UserPokemonModel::find($myParty->fourth_user_pokemon_id)),
            self::setMyPokemon(UserPokemonModel::find($myParty->fifth_user_pokemon_id)),
            self::setMyPokemon(UserPokemonModel::find($myParty->sixth_user_pokemon_id)),
            $myParty->memo
        );
    }

    /**
     * 他ユーザー登録ポケモンEntity生成
     *
     * @param UserPokemonModel $userPokemon
     * @param string $favoriteStatus
     * @param string $modalButtonClass
     * @param string $modalName
     * @return UserPokemon
     */
    public static function setUserPokemon(
        UserPokemonModel $userPokemon,
        string $favoriteStatus,
        string $modalButtonClass,
        string $modalName
    )
    {
        return new UserPokemon(
            (int) $userPokemon->id,
            self::setPokemon(PokemonModel::find($userPokemon->pokemon->id)),
            self::setItem(ItemModel::find($userPokemon->item->id)),
            self::setUser(UserModel::find($userPokemon->user_id)),
            $userPokemon->pokemon_name,
            (int) $userPokemon->hp,
            (int) $userPokemon->attack,
            (int) $userPokemon->defense,
            (int) $userPokemon->special_attack,
            (int) $userPokemon->special_defense,
            (int) $userPokemon->agility,
            $userPokemon->memo,
            $favoriteStatus,
            $modalButtonClass,
            $modalName
        );
    }

    /**
     * お気に入りポケモンEntity生成
     *
     * @param FavoriteModel $favorite
     * @return FavoriteMyPokemon
     */
    public static function setFavoriteMyPokemon(FavoriteModel $favorite)
    {
        return new FavoriteMyPokemon(
            (int) $favorite->id,
            self::setUserPokemon(
                UserPokemonModel::find($favorite->userPokemon->id),
                FavoriteStatus::RELEASE_STATUS,
                HtmlAttributeConst::RELEASE_USER_POKEMON_MODAL_BUTTON_CLASS,
                ModalNameConst::RELEASE_MODAL_NAME
            )
        );
    }

    /**
     * 解除ポケモンEntity生成
     *
     * @param UserPokemonModel $releaseMyPokemon
     * @return ReleaseMyPokemon
     */
    public static function setReleaseMyPokemon(UserPokemonModel $releaseMyPokemon)
    {
        return new ReleaseMyPokemon(
            $releaseMyPokemon->pokemon_name
        );
    }

    /**
     * 他ユーザーパーティEntity生成
     *
     * @param PartyModel $userParty
     * @param string $favoriteStatus
     * @param string $modalButtonClass
     * @param string $modalName
     * @return UserParty
     */
    public static function setUserParty(
        PartyModel $userParty,
        string $favoriteStatus,
        string $modalButtonClass,
        string $modalName
    )
    {
        return new UserParty(
            (int) $userParty->id,
            self::setUser(UserModel::find($userParty->user_id)),
            $userParty->name,
            self::setUserPokemon(
                UserPokemonModel::find($userParty->first_user_pokemon_id),
                '',
                '',
                ''
            ),
            self::setUserPokemon(
                UserPokemonModel::find($userParty->second_user_pokemon_id),
                '',
                '',
                ''
            ),
            self::setUserPokemon(
                UserPokemonModel::find($userParty->third_user_pokemon_id),
                '',
                '',
                ''
            ),
            self::setUserPokemon(
                UserPokemonModel::find($userParty->fourth_user_pokemon_id),
                '',
                '',
                ''
            ),
            self::setUserPokemon(
                UserPokemonModel::find($userParty->fifth_user_pokemon_id),
                '',
                '',
                ''
            ),
            self::setUserPokemon(
                UserPokemonModel::find($userParty->sixth_user_pokemon_id),
                '',
                '',
                ''
            ),
            $userParty->memo,
            $favoriteStatus,
            $modalButtonClass,
            $modalName
        );
    }

    /**
     * お気に入りパーティEntity生成
     *
     * @param FavoritePartyModel $favoriteParty
     * @return FavoriteMyParty
     */
    public static function setFavoriteMyParty(FavoritePartyModel $favoriteParty)
    {
        return new FavoriteMyParty(
            $favoriteParty->id,
            self::setUserParty(
                PartyModel::find($favoriteParty->user_party_id),
                '',
                '',
                ''
            )
        );
    }

    /**
     * 解除パーティEntity生成
     *
     * @param PartyModel $releaseMyParty
     * @return ReleaseMyParty
     */
    public static function setReleaseMyParty(PartyModel $releaseMyParty)
    {
        return new ReleaseMyParty(
            $releaseMyParty->name
        );
    }

    /**
     * ポケモンマスタEntity生成
     *
     * @param PokemonModel $pokemon
     * @return Pokemon
     */
    public static function setPokemon(PokemonModel $pokemon)
    {
        return new Pokemon(
            (int) $pokemon->id,
            $pokemon->name
        );
    }

    /**
     * アイテムマスタEntity生成
     *
     * @param ItemModel $item
     * @return Item
     */
    public static function setItem(ItemModel $item)
    {
        return new Item(
            (int) $item->id,
            $item->name
        );
    }

    /**
     * ユーザーEntity生成
     *
     * @param UserModel $user
     * @return User
     */
    public static function setUser(UserModel $user)
    {
        return new User(
            (int) $user->id,
            $user->name,
            $user->email,
            $user->password,
            UserType::judgeUserType($user->user_type)
        );
    }

    /**
     * お知らせEntity生成
     *
     * @param NoticeModel $notice
     * @return Notice
     */
    public static function setNotice(NoticeModel $notice)
    {
        return new Notice(
            (int) $notice->id,
            $notice->body,
            $notice->created_at->format('Y/m/d')
        );
    }
}